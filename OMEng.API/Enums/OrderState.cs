﻿namespace OMEng.API.Enums
{
  public enum OrderState
  {
    Pending = 0,
    Confirmed = 1,
    Shipped = 2
  }
}
