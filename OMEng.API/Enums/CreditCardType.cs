﻿namespace OMEng.API.Enums
{
  public enum CreditCardType
  {
    Visa = 0,
    Diners_Club = 1,
    American_Express = 2,
    Mastercard = 3
  }
}
