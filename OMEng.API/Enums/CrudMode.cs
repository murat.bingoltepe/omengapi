﻿namespace OMEng.API.Enums
{
  public enum CrudMode
  {
    Create,
    Update,
    Read,
    Delete
  }
}
