﻿namespace OMEng.API.Enums
{
  public enum UserRoles
  {
    SuperAdmin = 1001,
    Admin = 2001,
    Editor = 3001,
    WarehouseModerator = 4001,
    WarehouseStaff = 5001,
    ProductSalesman = 6001,
    Customer = 7001
  }
}