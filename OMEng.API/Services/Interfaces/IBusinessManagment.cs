﻿using OMEng.API.Dtos.CategoryDtos;
using OMEng.API.Dtos.CustomerDtos;
using OMEng.API.Dtos.EmployeeDtos;
using OMEng.API.Dtos.ProductDtos;
using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Services.Interfaces
{
  public interface IBusinessManagment
  {
    #region Employee
    Task<Employee> GetSuperAdminAsync(bool include = false);
    Task<ICollection<Employee>> GetEmployeesAsync(bool include = false);
    Task<Employee> GetEmployeeByIdAsync(int employeeId, bool include = false);
    Task<ICollection<Employee>> GetEmployeesByRoleIdAsync(int roleId, bool include = false);
    Task<Employee> GetEmployeeByUserNameAsync(string userName, bool include = false);
    Task<Employee> AddAsync(EmployeeForAddDto employeeForAddDto);
    Task<Employee> UpdateAsync(EmployeeForUpdateDto employeeForUpdateDto);
    Task<bool> EmployeeExistAsync(string employeeName);
    Task<bool> EmployeeExistAsync(int employeeId);

    /// <summary>
    /// Employee wird durch employeeId gelöscht
    /// </summary>
    /// <remarks>
    /// Before diese Method verwendet wird, es soll festgelegt werden
    /// dass der Employee schon existiert.
    /// </remarks>
    /// <see cref="EmployeesController die Method DeleteByIdAsync"/>
    /// <param name="employeeId">employeeId für die gelöscht werden soll.</param>
    void DeleteEmployeeByIdAsync(int employeeId);
    #endregion

    #region Customer
    Task<ICollection<Customer>> GetCustomersAsync(bool include = false);
    Task<Customer> GetCustomerByIdAsync(int customerId, bool include = false);
    Task<Customer> GetCustomerByUserNameAsync(string userName, bool include = false);
    Task<Customer> AddAsync(CustomerForAddDto customerForAddDto);
    Task<Customer> UpdateAsync(CustomerForUpdateDto customerForUpdateDto);
    Task<bool> CustomerExistAsync(string customerName);
    Task<bool> CustomerExistAsync(int customerId);

    /// <summary>
    /// Customer wird durch customerId gelöscht
    /// </summary>
    /// <remarks>
    /// Before diese Method verwendet wird, es soll festgelegt werden
    /// dass der Customer schon existiert.
    /// </remarks>
    /// <see cref="CustomersController die Method DeleteByIdAsync"/>
    /// <param name="customerId">customerId für die gelöscht werden soll.</param>
    void DeleteCustomerByIdAsync(int customerId);
    #endregion

    #region Product
    Task<ICollection<Product>> GetProductsAsync(bool include = false);
    Task<Product> GetProductByIdAsync(int productId, bool include = false);
    Task<ICollection<Product>> GetProductsByEmployeeIdAsync(int employeeId, bool include = false);
    Task<ICollection<Product>> GetProductsByCategoryIdAsync(int categoryId, bool include = false);
    Task<Product> AddAsync(ProductForAddDto productForAddDto);
    Task<Product> UpdateAsync(ProductForUpdateDto productForUpdateDto);
    Task<bool> ProductExistAsync(int productId);
    Task<bool> DeleteProductByIdAsync(int productId);
    #endregion

    #region Category
    Task<ICollection<Category>> GetCategoriesAsync(bool include = false);
    Task<ICollection<Category>> GetMainParentsAsync(bool include = false);
    Task<Category> GetCategoryByIdAsync(int categoryId, bool include = false);
    Task<ICollection<Category>> GetCategoriesByParentIdAsync(int parentId, bool include = false);
    Task<Category> AddAsync(CategoryForAddDto categoryForAddDto);
    void AddRange(ICollection<CategoryForAddDto> categories);
    Task<Category> UpdateAsync(CategoryForUpdateDto categoryForUpdateDto);
    void UpdateRange(ICollection<CategoryForUpdateDto> categories);
    Task<bool> CategoryExistAsync(int categoryId);
    void DeleteCategoryByIdAsync(int categoryId);
    void DeleteCategoryRange(ICollection<Category> categories);
    #endregion
  }
}
