﻿using OMEng.API.Data;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.CategoryDtos;
using OMEng.API.Dtos.CustomerDtos;
using OMEng.API.Dtos.EmployeeDtos;
using OMEng.API.Dtos.ProductDtos;
using OMEng.API.Models;
using OMEng.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Services.Implements
{
	public class BusinessManagment : IBusinessManagment
	{
		private readonly IAppRepository _appRepository;
		private readonly IEmployeeRepository _employeeRepository;
		private readonly ICustomerRepository _customerRepository;
		private readonly IUserRoleRepository _userRoleRepository;
		private readonly IProductRepository _productRepository;
		private readonly IPhotoRepository _photoRepository;
		private readonly IAddressRepository _addressRepository;
		private readonly IAccountRepository _accountRepository;
		private readonly ICategoryRepository _categoryRepository;


		public BusinessManagment(DataContext context,
														 IAppRepository appRepository, IEmployeeRepository employeeRepository,
														 ICustomerRepository customerRepository,
														 IUserRoleRepository userRoleRepository, IProductRepository productRepository,
														 IPhotoRepository photoRepository, IAddressRepository addressRepository,
														 IAccountRepository accountRepository, ICategoryRepository categoryRepository)
		{
			_appRepository = appRepository;
			_employeeRepository = employeeRepository;
			_customerRepository = customerRepository;
			_userRoleRepository = userRoleRepository;
			_productRepository = productRepository;
			_photoRepository = photoRepository;
			_addressRepository = addressRepository;
			_accountRepository = accountRepository;
			_categoryRepository = categoryRepository;
		}

		#region Employee  
		public async Task<Employee> GetSuperAdminAsync(bool include = false)
		{
			return await _employeeRepository.GetSuperAdminAsync(include);
		}
		public async Task<ICollection<Employee>> GetEmployeesAsync(bool include = false)
		{
			return await _employeeRepository.GetEmployeesAsync(include);
		}

		public async Task<Employee> GetEmployeeByIdAsync(int employeeId, bool include = false)
		{
			return await _employeeRepository.GetEmployeeByIdAsync(employeeId, include);
		}

		public async Task<ICollection<Employee>> GetEmployeesByRoleIdAsync(int roleId, bool include = false)
		{
			return await _employeeRepository.GetEmployeesByRoleIdAsync(roleId, include);
		}

		public async Task<Employee> GetEmployeeByUserNameAsync(string userName, bool include = false)
		{
			return await _employeeRepository.GetEmployeeByUserNameAsync(userName, include);
		}

		public async Task<Employee> AddAsync(EmployeeForAddDto employeeForAddDto)
		{
			return await _employeeRepository.AddAsync(employeeForAddDto);
		}

		public async Task<Employee> UpdateAsync(EmployeeForUpdateDto employeeForUpdateDto)
		{
			return await _employeeRepository.UpdateAsync(employeeForUpdateDto);
		}

		public async Task<bool> EmployeeExistAsync(string employeeName)
		{
			return await _employeeRepository.EmployeeExistAsync(employeeName);
		}

		public async Task<bool> EmployeeExistAsync(int employeeId)
		{
			return await _employeeRepository.EmployeeExistAsync(employeeId);
		}

		public async void DeleteEmployeeByIdAsync(int employeeId)
		{

			// ProduktPhotos von diesem Employee sollen zum SuperAdmin übertragen werden.      
			await AssignEmployeeDatasToSuperAdminAsync<Photo>(employeeId);

			// Produkten von diesem Employee sollen zum SuperAdmin übertragen werden.      
			await AssignEmployeeDatasToSuperAdminAsync<Product>(employeeId);

			// Employee wird gelöscht      
			Employee employee = await _employeeRepository.GetEmployeeByIdAsync(employeeId, true);
			_employeeRepository.Delete(employee);

			//Address von Employee soll auch gelöscht werden     
			_addressRepository.Delete(employee.Address);

			//Account von Employee soll auch gelöscht werden     
			_accountRepository.Delete(employee.Account);
		}

		private async Task AssignEmployeeDatasToSuperAdminAsync<T>(int employeeId)
		{
			// SuperAdmin
			Employee superAdmin = await _employeeRepository.GetSuperAdminAsync();

			if (typeof(T) == typeof(Photo))
			{
				ICollection<Photo> photos = await _photoRepository.GetPhotosByEmployeeIdAsync(employeeId);
				photos.ToList().ForEach(p => p.EmployeeId = superAdmin.Id);
				photos.ToList().ForEach(p => p.Employee = superAdmin);
				_photoRepository.UpdateRange(photos);
			}
			else if (typeof(T) == typeof(Product))
			{
				ICollection<Product> products = await _productRepository.GetProductsByEmployeeIdAsync(employeeId);
				products.ToList().ForEach(p => p.EmployeeId = superAdmin.Id);
				products.ToList().ForEach(p => p.Employee = superAdmin);
				_productRepository.UpdateRange(products);
			}
		}
		#endregion

		#region Customer
		public async Task<ICollection<Customer>> GetCustomersAsync(bool include = false)
		{
			return await _customerRepository.GetCustomersAsync(include);
		}

		public async Task<Customer> GetCustomerByIdAsync(int customerId, bool include = false)
		{
			return await _customerRepository.GetCustomerByIdAsync(customerId, include);
		}

		public async Task<Customer> GetCustomerByUserNameAsync(string userName, bool include = false)
		{
			return await _customerRepository.GetCustomerByUserNameAsync(userName, include);
		}

		public async Task<Customer> AddAsync(CustomerForAddDto customerForAddDto)
		{
			return await _customerRepository.AddAsync(customerForAddDto);
		}

		public async Task<Customer> UpdateAsync(CustomerForUpdateDto customerForUpdateDto)
		{
			return await _customerRepository.UpdateAsync(customerForUpdateDto);
		}

		public async Task<bool> CustomerExistAsync(string customerName)
		{
			return await _customerRepository.CustomerExistAsync(customerName);
		}

		public async Task<bool> CustomerExistAsync(int customerId)
		{
			return await _customerRepository.CustomerExistAsync(customerId);
		}

		public async void DeleteCustomerByIdAsync(int customerId)
		{
			// To-do
			// Die Bestellungen von Customer sollen auch gelöscht werden.

			// Customer wird gelöscht      
			Customer customer = await _customerRepository.GetCustomerByIdAsync(customerId, true);
			_customerRepository.Delete(customer);

			//Address von Customer soll auch gelöscht werden     
			_addressRepository.Delete(customer.Address);

			//Account von Customer soll auch gelöscht werden     
			_accountRepository.Delete(customer.Account);
		}
		#endregion

		#region Product
		public async Task<ICollection<Product>> GetProductsAsync(bool include = false)
		{
			return await _productRepository.GetProductsAsync(include);
		}

		public async Task<Product> GetProductByIdAsync(int productId, bool include = false)
		{
			return await _productRepository.GetProductByIdAsync(productId, include);
		}

		public async Task<ICollection<Product>> GetProductsByEmployeeIdAsync(int employeeId, bool include = false)
		{
			return await _productRepository.GetProductsByEmployeeIdAsync(employeeId, include);
		}

		public async Task<ICollection<Product>> GetProductsByCategoryIdAsync(int categoryId, bool include = false)
		{
			return await _productRepository.GetProductsByCategoryIdAsync(categoryId, include);
		}
		public async Task<Product> AddAsync(ProductForAddDto productForAddDto)
		{
			return await _productRepository.AddAsync(productForAddDto);
		}

		public async Task<Product> UpdateAsync(ProductForUpdateDto productForUpdateDto)
		{
			return await _productRepository.UpdateAsync(productForUpdateDto);
		}

		public async Task<bool> ProductExistAsync(int productId)
		{
			return await _productRepository.ProductExistAsync(productId);
		}

		public async Task<bool> DeleteProductByIdAsync(int productId)
		{
			Product product = await _productRepository.GetProductByIdAsync(productId, true);

			bool success;
			try
			{
				_productRepository.Delete(product);
				success = false;
			}
			catch (Exception e)
			{
				throw e;
			}

			return success;
		}
		#endregion

		#region Category
		public async Task<ICollection<Category>> GetCategoriesAsync(bool include = false)
		{
			return await _categoryRepository.GetCategoriesAsync(include);
		}

		public async Task<ICollection<Category>> GetMainParentsAsync(bool include = false)
		{
			return await _categoryRepository.GetMainParentsAsync(include);
		}

		public async Task<Category> GetCategoryByIdAsync(int categoryId, bool include = false)
		{
			return await _categoryRepository.GetCategoryByIdAsync(categoryId, include);
		}

		public async Task<ICollection<Category>> GetCategoriesByParentIdAsync(int parentId, bool include = false)
		{
			return await _categoryRepository.GetCategoriesByParentIdAsync(parentId, include);
		}

		public async Task<Category> AddAsync(CategoryForAddDto categoryForAddDto)
		{
			return await _categoryRepository.AddAsync(categoryForAddDto);

		}

		public void AddRange(ICollection<CategoryForAddDto> categories)
		{
			_categoryRepository.AddRange(categories);
		}

		public async Task<Category> UpdateAsync(CategoryForUpdateDto categoryForUpdateDto)
		{
			return await _categoryRepository.UpdateAsync(categoryForUpdateDto);
		}

		public void UpdateRange(ICollection<CategoryForUpdateDto> categories)
		{
			_categoryRepository.UpdateRangeAsync(categories);
		}

		public async Task<bool> CategoryExistAsync(int categoryId)
		{
			return await _categoryRepository.CategoryExistAsync(categoryId);
		}

		public async void DeleteCategoryByIdAsync(int categoryId)
		{
			Category category = await _categoryRepository.GetCategoryByIdAsync(categoryId);
			
			// Kategorie wird gelöscht.
			_categoryRepository.Delete(category);

			// Untere Kategorien sollen auch gelöscht werden
			if (category.IsParent)
			{
				ICollection<Category> categories = await _categoryRepository.GetCategoriesByParentIdAsync(categoryId);
				_categoryRepository.DeleteRange(categories);
			}

			// Es kann sein dass die gelöschte Kategorie letzte Kategorie von parent Kategorie ist.
			// Wenn Ja, soll die isParent Property von parent Kategorie auf false ersetzt werden.
			IfNecessarySetParentPropertyOfCategory((int)category.ParentId);
		}
		public void DeleteCategoryRange(ICollection<Category> categories)
		{
			_categoryRepository.DeleteRange(categories);
		}

		/// <summary>
		/// Es wird überürft ob die Kategorie untere Kategorien hat oder nicht.
		/// Wenn keine untere Kategories gibt, wird die isParent Property von
		/// Kategorie auf False ersetzt.
		/// </summary>
		/// <param name="categoryId">Durch categoryId wird Kategorie überprüft und ersetzt.</param>
		private async void IfNecessarySetParentPropertyOfCategory(int categoryId)
		{
			Category category = await _categoryRepository.GetCategoryByIdAsync(categoryId);

			if (category.IsParent)
			{
				ICollection<Category> categories = await _categoryRepository.GetCategoriesByParentIdAsync(categoryId);

				if (categories.Count == 0)
				{
					CategoryForUpdateDto categoryForUpdateDto = new CategoryForUpdateDto
					{
						Id = category.Id,
						Name = category.Name,
						Description = category.Description,
						ParentId = category.ParentId,
						IsParent = false												
					};

					await _categoryRepository.UpdateAsync(categoryForUpdateDto);
				}
			}

		}
		#endregion
	}
}