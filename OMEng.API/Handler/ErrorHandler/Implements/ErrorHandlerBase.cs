﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OMEng.API.Data;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Data.Repository;
using OMEng.API.Enums;
using OMEng.API.Models;

namespace OMEng.API.Handler.ErrorHandler.Implements
{
  public class ErrorHandlerBase
  {
    private static readonly DataContext context = new DataContext(new DbContextOptions<DataContext>());
    private static readonly IAppRepository appRepository = new AppRepository(context);
    private static readonly IEmployeeRepository employeeRepository = new EmployeeRepository(context, appRepository);
    private static readonly ICustomerRepository customerRepository = new CustomerRepository(context, appRepository);
    private static readonly IProductRepository productRepository = new ProductRepository(context, appRepository);
    private static readonly ICategoryRepository categoryRepository = new CategoryRepository(context, appRepository);
    private static readonly IUserRoleRepository userRoleRepository = new UserRoleRepository(context);

    protected Dictionary<string, string> errorList = new Dictionary<string, string>();

    public async Task<Dictionary<string, string>> CheckUserExistByUserNameAsync<T>(string userName, CrudMode mode)
    {
      bool exist = false;
      string key = "";
      if (typeof(T) == typeof(Employee))
      {
        key = "Employee";
        exist = await employeeRepository.EmployeeExistAsync(userName);
      }
      else if (typeof(T) == typeof(Customer))
      {
        key = "Customer";
        exist = await customerRepository.CustomerExistAsync(userName);
      }


      if (exist)
      {
        if (mode.Equals(CrudMode.Create))
        {
          errorList.Add(key, key + " already exist");
        }
      }

      return errorList;
    }

    public async Task<Dictionary<string, string>> CheckUserExistByIdAsync<T>(int id)
    {
      string key = "";
      bool exist = false;
      if (typeof(T) == typeof(Employee))
      {
        key = "Employee";
        exist = await employeeRepository.EmployeeExistAsync(id);
      }
      else if (typeof(T) == typeof(Customer))
      {
        key = "Customer";
        exist = await customerRepository.CustomerExistAsync(id);
      }

      if (!exist)
      {
        errorList.Add(key, key + " does not exist");
      }

      return errorList;
    }

    public async Task<Dictionary<string, string>> CheckUserEmailExistAsync<T>(string email)
    {
      bool exist = false;
      if (typeof(T) == typeof(Employee))
      {
        exist = await employeeRepository.EmailExistAsync(email);
      }
      else if (typeof(T) == typeof(Customer))
      {
        exist = await customerRepository.EmailExistAsync(email);
      }


      if (exist)
      {
        errorList.Add("Email", "Email already exist");
      }

      return errorList;
    }

    public async Task<Dictionary<string, string>> CheckIdOfEntityExistAsync<T>(int id)
    {
      string key = "";
      bool exist = false;
      if (typeof(T) == typeof(Product))
      {
        key = "Product";
        exist = await productRepository.ProductExistAsync(id);
      }
      else if (typeof(T) == typeof(Category))
      {
        key = "Category";
        exist = await categoryRepository.CategoryExistAsync(id);
      }

      if (!exist)
      {
        if (key == "Category")
        {
          errorList.Add(key, key + " resp. ParentId does not exist");
        }
        else
        {
          errorList.Add(key, key + " does not exist");
        }

      }

      return errorList;
    }

    public async Task<Dictionary<string, string>> CheckUserRoleAsync(Employee employee, CrudMode mode)
    {
      switch (employee?.UserRoleId)
      {
        case 0:
          errorList.Add("Employee.UserRole", "UserRole cannot be 0");
          break;
        case (int)UserRoles.Customer:
          if (mode.Equals(CrudMode.Create) || mode.Equals(CrudMode.Update))
          {
            errorList.Add("Employee.UserRole", "The employee's role can not be saved as a customer. ");
          }
          else if (mode.Equals(CrudMode.Read))
          {
            errorList.Add("Employee.UserRole", "The employee's can not with customer role id loaded. ");
          }
          break;
        case (int)UserRoles.SuperAdmin:
          if (mode.Equals(CrudMode.Delete))
          {
            errorList.Add("Employee.UserRole", "The Employee has UserRole as SuperAdmin and this may not deleted. ");
          }
          break;
        default:
          {
            ICollection<UserRole> userRoles = await userRoleRepository.GetUserRolesAsync();
            ICollection<int> userRoleIds = userRoles.Select(u => u.Id).ToList();
            if (!userRoleIds.Contains(employee.UserRoleId))
            {
              errorList.Add("Employee.UserRole", "This UserRole Id does not exist");
            }
            break;
          }
      }

      return errorList;
    }

    public Dictionary<string, string> CheckAddress<T>(T user)
    {
      dynamic userToCheck = null;
      string key = "";
      if (typeof(T) == typeof(Employee))
      {
        key = "Employee";
        userToCheck = user as Employee;
      }
      else if (typeof(T) == typeof(Customer))
      {
        key = "Customer";
        userToCheck = user as Customer;
      }

      if (userToCheck?.Address == null)
      {
        errorList.Add(key + ".Address", "Address cannot be null");
      }

      return errorList;
    }

    public Dictionary<string, string> CheckAccount<T>(T user)
    {
      dynamic userToCheck = null;
      string key = "";
      if (typeof(T) == typeof(Employee))
      {
        key = "Employee";
        userToCheck = user as Employee;
      }
      else if (typeof(T) == typeof(Customer))
      {
        key = "Customer";
        userToCheck = user as Customer;
      }

      if (userToCheck?.Account == null)
      {
        errorList.Add(key + ".Account", "Account cannot be null");
      }

      return errorList;
    }

  }
}
