﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OMEng.API.Enums;
using OMEng.API.Models;

namespace OMEng.API.Handler.ErrorHandler.Implements
{
  public class ErrorHandler
  {
    #region Employee
    public static async Task<Dictionary<string, string>> CheckEmployeeForByIdAsync(Employee employee)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (employee == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Employee>(employee.Id);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckEmployeeForListByRoleIdAsync(Employee employee)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (employee == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserRoleAsync(employee, CrudMode.Read);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckEmployeeForAddAsync(Employee employee)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (employee == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByUserNameAsync<Employee>(employee.UserName, CrudMode.Create);
        errorList = await errorHandlerBase.CheckUserEmailExistAsync<Employee>(employee.Email);
        errorList = await errorHandlerBase.CheckUserRoleAsync(employee, CrudMode.Create);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckEmployeeForUpdateAsync(Employee employee)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (employee == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        
        // Todo Beim Update soll Id und Email nicht kontrolliert werden.
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Employee>(employee.Id);
        errorList = await errorHandlerBase.CheckUserEmailExistAsync<Employee>(employee.Email);
        errorList = await errorHandlerBase.CheckUserRoleAsync(employee, CrudMode.Update);
        errorList = errorHandlerBase.CheckAddress(employee);
        errorList = errorHandlerBase.CheckAccount(employee);
      }

      return errorList;
    }   

    public static async Task<Dictionary<string, string>> CheckEmployeeForDeleteAsync(Employee employee)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (employee == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Employee>(employee.Id);

        if (errorList.Count == 0)
        {          
          errorList = await errorHandlerBase.CheckUserRoleAsync(employee, CrudMode.Delete);
        }        
      }

      return errorList;
    }
    #endregion

    #region Customer
    public static async Task<Dictionary<string, string>> CheckCustomerForByIdAsync(Customer customer)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (customer == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Customer>(customer.Id);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckCustomerForAddAsync(Customer customer)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (customer == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByUserNameAsync<Customer>(customer.UserName, CrudMode.Create);
        errorList = await errorHandlerBase.CheckUserEmailExistAsync<Customer>(customer.Email);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckCustomerForUpdateAsync(Customer customer)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (customer == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Customer>(customer.Id);
        errorList = await errorHandlerBase.CheckUserEmailExistAsync<Customer>(customer.Email);
        errorList = errorHandlerBase.CheckAddress(customer);
        errorList = errorHandlerBase.CheckAccount(customer);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckCustomerForDeleteAsync(Customer customer)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (customer == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Customer>(customer.Id);        
      }

      return errorList;
    }
    #endregion

    #region Product
    public static async Task<Dictionary<string, string>> CheckProductForByIdAsync(Product product)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (product == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Product>(product.Id);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckProductForByEmployeeIdAsync(Product product)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (product == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Employee>(product.EmployeeId);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckProductForByCategoryIdAsync(Product product)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (product == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>(product.CategoryId);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckProductForAddAsync(Product product)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (product == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Employee>(product.EmployeeId);
        errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>(product.CategoryId);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckProductForUpdateAsync(Product product)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (product == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Product>(product.Id);
        errorList = await errorHandlerBase.CheckUserExistByIdAsync<Employee>(product.EmployeeId);
        errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>(product.CategoryId);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckProductForDeleteAsync(Product product)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (product == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Product>(product.Id);
      }

      return errorList;
    }
    #endregion

    #region Category
    public static async Task<Dictionary<string, string>> CheckCategoryForByIdAsync(Category category)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (category == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>(category.Id);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckCategoryForParentIdAsync(Category category)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (category == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        // Jeder ParentId ist ein CategoryId. Deswegen für ErrorHandler man kann 
        // die Methode CheckIdOfEntityExistAsync verwenden.
        if ((int)category.ParentId != 0)
        {
          errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>((int)category.ParentId);
        }
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckCategoryForAddAndUpdateAsync(Category category)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (category == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        
        if ((int)category.ParentId != 0 )
        {
          errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>((int)category.ParentId);
        }        
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckCategoryForAddAndUpdateAsync(ICollection<Category> categories)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (categories.Count == 0)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        foreach (Category category in categories)
        {
          if ((int)category.ParentId != 0)
          {
            errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>((int)category.ParentId);
          }          

          if (errorList.Count > 0)
          {
            break;
          }
        }        
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckCategoryForDeleteAsync(Category category)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (category == null)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>(category.Id);
      }

      return errorList;
    }

    public static async Task<Dictionary<string, string>> CheckCategoryForDeleteAsync(ICollection<Category> categories)
    {
      Dictionary<string, string> errorList = new Dictionary<string, string>();

      if (categories.Count == 0)
      {
        errorList.Add("Error", "Value cannot be null");
      }
      else
      {
        ErrorHandlerBase errorHandlerBase = new ErrorHandlerBase();
        foreach (Category category in categories)
        {
          errorList = await errorHandlerBase.CheckIdOfEntityExistAsync<Category>(category.Id);
          if (errorList.Count > 0)
          {
            break;
          }
        }        
      }

      return errorList;
    }
    #endregion
  }
}
