﻿using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IAddressRepository
  {
    Task<ICollection<Address>> GetAddressesAsync();
    Task<Address> GetAddressByIdAsync(int addressId);
    void Delete(Address address);
  }
}
