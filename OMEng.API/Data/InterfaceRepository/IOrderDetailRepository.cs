﻿using OMEng.API.Dtos.OrderDetailDtos;
using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
	interface IOrderDetailRepository
	{
		Task<ICollection<OrderDetail>> GetOrderDetailsAsync(bool include = false);
		ICollection<OrderDetail> GetOrderDetails(bool include = false);

		Task<OrderDetail> GetOrderDetailByIdAsync(int orderDetailId, bool include = false);
		OrderDetail GetOrderDetailById(int orderDetailId, bool include = false);

		Task<ICollection<OrderDetail>> GetOrderDetailsByOrderIdAsync(int orderId, bool include = false);
		
		Task<ICollection<OrderDetail>> GetOrderDetailsByProductIdAsync(int productId, bool include = false);

		Task<bool> AddAsync(OrderDetailForAddDto orderDetailForAddDto);

		bool Update(OrderDetailForUpdate orderDetailForUpdate);
	}
}
