﻿using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IUserRoleRepository
  {
    Task<ICollection<UserRole>> GetUserRolesAsync();
    Task<UserRole> GetUserRoleByIdAsync(int userRoleId);
  }
}
