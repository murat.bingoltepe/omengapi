﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IAppRepository
  {
    Task AddAsync<T>(T entity) where T : class;
    void AddRange<T>(ICollection<T> entities) where T : class;
    Task SaveAllAsync();
    void Update<T>(T entity) where T : class;
    void UpdateRange<T>(ICollection<T> entities) where T : class;    
    void Delete<T>(T entity) where T : class;
    void DeleteRange<T>(ICollection<T> entities) where T : class;
    bool SaveAll();    
  }
}
