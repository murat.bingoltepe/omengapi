﻿using OMEng.API.Models;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IEmployeeAuthRepository
  {   
    Task<Employee> Login(string userName, string password);   
    Task<Employee> ChangePassword(Employee employee);
  }
}
