﻿using OMEng.API.Models;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface ICustomerAuthRepository
  {
    Task<Customer> Register(Customer customer, string password);
    Task<Customer> Login(string userName, string password);
    Task<bool> UserExist(string userName);
    Task<Customer> ChangePassword(Customer customer);
  }
}
