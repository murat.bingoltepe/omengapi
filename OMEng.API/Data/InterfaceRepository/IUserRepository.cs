﻿using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IUserRepository
  {
    Task<ICollection<User>> GetUsersAsync();
    Task<User> GetUserByIdAsync(int userId);
    Task<ICollection<User>> GetUsersByRoleIdAsync(int roleId);
  }
}
