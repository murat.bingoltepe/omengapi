﻿using OMEng.API.Dtos.ProductDtos;
using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IProductRepository
  {
    Task<ICollection<Product>> GetProductsAsync(bool include = false);
    Task<Product> GetProductByIdAsync(int productId, bool include = false);
    Task<ICollection<Product>> GetProductsByEmployeeIdAsync(int employeeId, bool include = false);
    Task<ICollection<Product>> GetProductsByCategoryIdAsync(int categoryId, bool include = false);
    Task<Product> AddAsync(ProductForAddDto productForAddDto);
    Task<Product> UpdateAsync(ProductForUpdateDto productForUpdateDto);
    void UpdateRange(ICollection<Product> products);
    Task<bool> ProductExistAsync(int productId);
    void Delete(Product product);
  }
}
