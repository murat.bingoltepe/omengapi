﻿using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IAccountRepository
  {
    Task<ICollection<Account>> GetAccountsAsync();
    Task<Account> GetAccountsByIdAsync(int accountId);
    void Delete(Account account);
  }
}
