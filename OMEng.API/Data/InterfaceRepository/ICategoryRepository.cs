﻿using OMEng.API.Dtos.CategoryDtos;
using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface ICategoryRepository
  {
    Task<ICollection<Category>> GetCategoriesAsync(bool include = false);
    Task<ICollection<Category>> GetMainParentsAsync(bool include = false);
    Task<Category> GetCategoryByIdAsync(int categoryId, bool include = false);
    Task<ICollection<Category>> GetCategoriesByParentIdAsync(int parentId, bool include = false);
    Task<Category> AddAsync(CategoryForAddDto categoryForAddDto);
    void AddRange(ICollection<CategoryForAddDto> categories);
    Task<Category> UpdateAsync(CategoryForUpdateDto categoryForUpdateDto);
    void UpdateRangeAsync(ICollection<CategoryForUpdateDto> categories);
    Task<bool> CategoryExistAsync(int categoryId);
    void Delete(Category category);
    void DeleteRange(ICollection<Category> categories);
  }
}
