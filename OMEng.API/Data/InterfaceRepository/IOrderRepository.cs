﻿using OMEng.API.Dtos.OrderDtos;
using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
	interface IOrderRepository
	{
		Task<ICollection<Order>> GetOrdersAsync(bool include = false);
		ICollection<Order> GetOrders(bool include = false);

		Task<Order> GetOrderByIdAsync(int orderId, bool include = false);
		Order GetOrderById(int orderId, bool include = false);

		Task<ICollection<Order>> GetOrdersByCustomerIdAsync(int customerId, bool include = false);

		Task<bool> AddAsync(OrderForAddDto orderForAddDto);

		bool Update(OrderForUpdateDto orderForUpdateDto);

		bool Delete(Order order);
	}
}
