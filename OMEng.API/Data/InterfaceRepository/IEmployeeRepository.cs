﻿using OMEng.API.Dtos.EmployeeDtos;
using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IEmployeeRepository
  {
    Task<Employee> GetSuperAdminAsync(bool include = false);
    Task<ICollection<Employee>> GetEmployeesAsync(bool include = false);    
    Task<Employee> GetEmployeeByIdAsync(int employeeId, bool include = false);
    Task<ICollection<Employee>> GetEmployeesByRoleIdAsync(int roleId, bool include = false);
    Task<Employee> GetEmployeeByUserNameAsync(string userName, bool include = false);
    Task<Employee> AddAsync(EmployeeForAddDto employeeForAddDto);
    Task<Employee> UpdateAsync(EmployeeForUpdateDto employeeForUpdateDto);    
    Task<bool> EmployeeExistAsync(string employeeName);
    Task<bool> EmployeeExistAsync(int employeeId);
    Task<bool> EmailExistAsync(string email);
    void Delete(Employee employee);
  }
}
