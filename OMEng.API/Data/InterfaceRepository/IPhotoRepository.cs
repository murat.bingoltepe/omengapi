﻿using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface IPhotoRepository
  {
    Task<ICollection<Photo>> GetPhotosAsync();
    Task<Photo> GetPhotoByIdAsync(int photoId);
    Task<ICollection<Photo>> GetPhotosByEmployeeIdAsync(int employeeId);
    Task<ICollection<Photo>> GetPhotosWithDetailByEmployeeIdAsync(int employeeId);
    Task<ICollection<Photo>> GetPhotosByProductIdAsync(int productId);
    Photo Update(Photo photo);
    void UpdateRange(ICollection<Photo> photos);
  }
}
