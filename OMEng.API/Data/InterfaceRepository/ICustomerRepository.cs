﻿using OMEng.API.Dtos.CustomerDtos;
using OMEng.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.InterfaceRepository
{
  public interface ICustomerRepository
  {
    Task<ICollection<Customer>> GetCustomersAsync(bool include = false);
    Task<Customer> GetCustomerByIdAsync(int customerId, bool include = false);
    Task<Customer> GetCustomerByUserNameAsync(string userName, bool include = false);
    Task<Customer> AddAsync(CustomerForAddDto customerForAddDto);
    Task<Customer> UpdateAsync(CustomerForUpdateDto customerForUpdateDto);
    Task<bool> CustomerExistAsync(string customerName);
    Task<bool> CustomerExistAsync(int customerId);
    Task<bool> EmailExistAsync(string email);
    void Delete(Customer customer);
  }
}
