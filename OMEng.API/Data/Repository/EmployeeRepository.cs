﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.EmployeeDtos;
using OMEng.API.Enums;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class EmployeeRepository : IEmployeeRepository
  {
    private readonly DataContext _context;
    private readonly IAppRepository _appRepository;

    public EmployeeRepository(DataContext context, IAppRepository appRepository)
    {
      _context = context;
      _appRepository = appRepository;
    }

    public async Task<Employee> GetSuperAdminAsync(bool include = false)
    {
      Employee superAdmin;

      try
      {
        if (include)
        {
          superAdmin = await _context.Employees
          .Include(e => e.UserRole)
          .Include(e => e.Address)
          .Include(e => e.Products)
          .Include(e => e.Account)
          .Where(e => e.UserRoleId == (int)UserRoles.SuperAdmin).FirstOrDefaultAsync();
        }
        else
        {
          superAdmin = await _context.Employees.Where(e => e.UserRoleId == (int)UserRoles.SuperAdmin).FirstOrDefaultAsync();
        }
      }
      catch (Exception e)
      {
        throw e;
      }

      return superAdmin;
    }

    public async Task<ICollection<Employee>> GetEmployeesAsync(bool include = false)
    {
      ICollection<Employee> employees;

      try
      {
        if (include)
        {
          employees = await _context.Employees
          .Include(e => e.UserRole)
          .Include(e => e.Address)
          .Include(e => e.Products)
          .Include(e => e.Account)
          .ToListAsync();
        }
        else
        {
          employees = await _context.Employees.ToListAsync();
        }
      }
      catch (Exception e)
      {
        throw e;
      }

      return employees;
    }

    public async Task<Employee> GetEmployeeByIdAsync(int employeeId, bool include = false)
    {
      Employee employee;

      try
      {
        if (include)
        {
          employee = await _context.Employees
         .Include(e => e.UserRole)
         .Include(e => e.Address)
         .Include(e => e.Products)
         .Include(e => e.Account)
         .FirstOrDefaultAsync(e => e.Id == employeeId);
        }
        else
        {
          employee = await _context.Employees.FirstOrDefaultAsync(e => e.Id == employeeId);
        }
      }
      catch (Exception e)
      {
        throw e;
      }

      return employee;
    }

    public async Task<ICollection<Employee>> GetEmployeesByRoleIdAsync(int roleId, bool include = false)
    {
      ICollection <Employee> employees;

      try
      {
        if (include)
        {
          employees = await _context.Employees
            .Include(e => e.UserRole)
            .Include(e => e.Address)
            .Include(e => e.Products)
            .Include(e => e.Account)
            .Where(e => e.UserRoleId == roleId).ToListAsync();
        }
        else
        {
          employees = await _context.Employees.Where(e => e.UserRoleId == roleId).ToListAsync();
        }
      }
      catch (Exception e)
      {
        throw e;
      }

      return employees;
    }

    public async Task<Employee> GetEmployeeByUserNameAsync(string userName, bool include = false)
    {
      Employee employee;

      try
      {
        if (include)
        {
          employee = await _context.Employees
            .Include(e => e.UserRole)
            .Include(e => e.Address)
            .Include(e => e.Products)
            .Include(e => e.Account)
            .FirstOrDefaultAsync(e => e.UserName.ToLower() == userName.ToLower());
        }
        else
        {
          employee = await _context.Employees.FirstOrDefaultAsync(e => e.UserName.ToLower() == userName.ToLower());
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return employee;
    }

    public async Task<Employee> AddAsync(EmployeeForAddDto employeeForAddDto)
    {
      Employee employee = new Employee();
      _context.Entry(employee).CurrentValues.SetValues(employeeForAddDto);

      employee.Address = CheckAddressForAdd(employeeForAddDto.Address);
      employee.Account = CheckAccountForAdd(employeeForAddDto.Account);

      employee.TempPassword = true;
      employee.EmailConfirmed = false;

      byte[] passwordHash;
      byte[] passwordSalt;

      CreatePasswordHash(employeeForAddDto.Password, out passwordHash, out passwordSalt);

      employee.PasswordHash = passwordHash;
      employee.PasswordSalt = passwordSalt;

      try
      {
        await _appRepository.AddAsync(employee);
        await _appRepository.SaveAllAsync();

        return employee;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<Employee> UpdateAsync(EmployeeForUpdateDto employeeForUpdateDto)
    {

      Employee employee = await GetEmployeeByIdAsync(employeeForUpdateDto.Id, true);

      _context.Entry(employee).CurrentValues.SetValues(employeeForUpdateDto);
      _context.Entry(employee.Address).CurrentValues.SetValues(employeeForUpdateDto.Address);
      _context.Entry(employee.Account).CurrentValues.SetValues(employeeForUpdateDto.Account);

      // AdressId soll in Address Entity über tragen werden.
      // Sons wird nicht korrekte Update.
      employee.Address.Id = employee.AddressId;

      // AccountId soll in Account Entity über tragen werden.
      // Sons wird nicht korrekte Update.
      employee.Account.Id = employee.AccountId;

      // Beim Update UserRole Entity darf nicht mit gegeben werden.
      // Es genug wen mann nun die UserRoleId mit schickt.
      employee.UserRole = null;

      try
      {
        _appRepository.Update(employee);
        _appRepository.SaveAll();

        return employee;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public void Delete(Employee employee)
    {
      try
      {        
        _appRepository.Delete(employee);        
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<bool> EmployeeExistAsync(string employeeName)
    {
      try
      {
        Employee existsEmployee = await GetEmployeeByUserNameAsync(employeeName);

        return existsEmployee != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<bool> EmployeeExistAsync(int employeeId)
    {
      try
      {
        Employee existsEmployee = await GetEmployeeByIdAsync(employeeId);

        return existsEmployee != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<bool> EmailExistAsync(string email)
    {
      try
      {
        Employee existsEmail = await _context.Employees.FirstOrDefaultAsync(e => e.Email.ToLower() == email.ToLower());

        return existsEmail != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
    {
      using (HMACSHA512 hmac = new HMACSHA512())
      {
        passwordSalt = hmac.Key;
        passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
      }
    }
    
    private Address CheckAddressForAdd(Address address)      
    {
      Address resultAddress = address;
      if (address == null)
      {
        resultAddress = new Address
        {
          City = string.Empty,
          Country = string.Empty,
          Number = string.Empty,
          PostalCode = string.Empty,
          Street = string.Empty
        };                    
      }

      return resultAddress;
    }

    private Account CheckAccountForAdd(Account account)
    {
      Account resultAccount = account;
      if (account == null)
      {
        resultAccount = new Account
        {
          AccountNumber = string.Empty,
          BankCode = string.Empty,
          BankName = string.Empty,
          Iban = string.Empty,
          Swift = string.Empty
        };
      }

      return resultAccount;
    }
  }
}
