﻿using OMEng.API.Data.InterfaceRepository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class AppRepository : IAppRepository
  {
    private readonly DataContext _context;

    public AppRepository(DataContext context)
    {
      this._context = context;
    }

    public async Task AddAsync<T>(T entity) where T : class
    {
      try
      {
        await this._context.AddAsync(entity);
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public void AddRange<T>(ICollection<T> entities) where T : class
    {
      try
      {
        this._context.AddRange(entities);
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public void Update<T>(T entity) where T : class
    {
      try
      {
        this._context.Update(entity);
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public void UpdateRange<T>(ICollection<T> entities) where T : class
    {
      try
      {
        this._context.UpdateRange(entities);
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public void Delete<T>(T entity) where T : class
    {
      try
      {
        this._context.Remove(entity);
      }
      catch (Exception e)
      {
        throw e;
      }
    }    

    public void DeleteRange<T>(ICollection<T> entities) where T : class
    {
      try
      {
        this._context.RemoveRange(entities);
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task SaveAllAsync()
    {
      try
      {
        await this._context.SaveChangesAsync();
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public bool SaveAll()
    {
      try
      {
        return _context.SaveChanges() > 0;
      }
      catch (Exception e)
      {
        throw e;
      }
    }    
  }
}
