﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class AddressRepository : IAddressRepository
  {
    private readonly DataContext _context;
    private readonly IAppRepository _appRepository;

    public AddressRepository(DataContext context, IAppRepository appRepository)
    {
      this._context = context;
      this._appRepository = appRepository;
    }
  
    public async Task<ICollection<Address>> GetAddressesAsync()
    {
      ICollection<Address> addresses;

      try
      {
        addresses = await this._context.Addresses.ToListAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return addresses;
    }

    public async Task<Address> GetAddressByIdAsync(int addressId)
    {
      Address address;

      try
      {
        address = await this._context.Addresses.FirstOrDefaultAsync(a => a.Id == addressId);
      }
      catch (Exception e)
      {
        throw e;
      }

      return address;
    }

    public void Delete(Address address)
    {

      try
      {
        this._appRepository.Delete(address);
        this._appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }
    }
  }
}
