﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.ProductDtos;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class ProductRepository : IProductRepository
  {
    private readonly DataContext _context;
    private readonly IAppRepository _appRepository;

    public ProductRepository(DataContext context, IAppRepository appRepository)
    {
      this._context = context;
      this._appRepository = appRepository;
    }

    public async Task<ICollection<Product>> GetProductsAsync(bool include = false)
    {
      ICollection<Product> products;

      try
      {
        if (include)
        {
          products = await this._context.Products
            .Include(p => p.Category)
            .Include(p => p.Employee)
            .Include(p => p.Photos)
            .ToListAsync();
        }
        else
        {
          products = await this._context.Products.ToListAsync();
        }
      }
      catch (Exception e)
      {        
        throw e;
      }

      return products;
    }

    public async Task<Product> GetProductByIdAsync(int productId, bool include = false)
    {
      Product product;

      try
      {
        if (include)
        {
          product = await this._context.Products
            .Include(p => p.Category)
            .Include(p => p.Employee)
            .Include(p => p.Photos)
            .FirstOrDefaultAsync(p => p.Id == productId);
        }
        else
        {
          product = await this._context.Products.FirstOrDefaultAsync(p => p.Id == productId);
        }
      }
      catch (Exception e)
      {
        throw e;
      }

      return product;
    }

    public async Task<ICollection<Product>> GetProductsByEmployeeIdAsync(int employeeId, bool include = false)
    {
      ICollection<Product> products;

      try
      {
        if (include)
        {
          products = await this._context.Products
            .Include(p => p.Category)
            .Include(p => p.Employee)
            .Include(p => p.Photos)
            .Where(p => p.EmployeeId == employeeId).ToListAsync();
        }
        else
        {
          products = await this._context.Products.Where(p => p.EmployeeId == employeeId).ToListAsync();
        }
      }
      catch (Exception e)
      {
        throw e;
      }

      return products;
    }

    public async Task<ICollection<Product>> GetProductsByCategoryIdAsync(int categoryId, bool include = false)
    {
      ICollection<Product> products;

      try
      {
        if (include)
        {
          products = await this._context.Products
            .Include(p => p.Category)
            .Include(p => p.Employee)
            .Include(p => p.Photos)
            .Where(p => p.CategoryId == categoryId).ToListAsync();
        }
        else
        {
          products = await this._context.Products.Where(p => p.CategoryId == categoryId).ToListAsync();
        }
      }
      catch (Exception e)
      {
        throw e;
      }

      return products;
    }

    public async Task<Product> AddAsync(ProductForAddDto productForAddDto)
    {
      Product product = new Product();
      _context.Entry(product).CurrentValues.SetValues(productForAddDto);

      try
      {
        await this._appRepository.AddAsync(product);
        await this._appRepository.SaveAllAsync();

        return product;
      }
      catch (Exception e)
      {
        throw e;
      }

    }

    public async Task<Product> UpdateAsync(ProductForUpdateDto productForUpdateDto)
    {
      Product product = await GetProductByIdAsync(productForUpdateDto.Id, true);
      _context.Entry(product).CurrentValues.SetValues(productForUpdateDto);

      // Beim Update Employee Entity darf nicht mit gegeben werden.
      // Es genug wen mann nun die EmployeeId mit schickt.
      product.Employee = null;

      // Beim Update Category Entity darf nicht mit gegeben werden.
      // Es genug wen mann nun die CategoryId mit schickt.
      product.Category = null;

      try
      {
        this._appRepository.Update(product);
        this._appRepository.SaveAll();

        return product;
      }
      catch (Exception e)
      {
        throw e;
      }

    }

    public void UpdateRange(ICollection<Product> products)
    {
      try
      {
        this._appRepository.UpdateRange(products);
        this._appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public void Delete(Product product)
    {
      try
      {
        this._appRepository.Delete(product);
        this._appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<bool> ProductExistAsync(int productId)
    {
      try
      {
        Product existsProduct = await this.GetProductByIdAsync(productId);

        return existsProduct != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }
   
  }
}
