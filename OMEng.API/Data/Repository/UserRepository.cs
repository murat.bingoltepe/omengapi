﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class UserRepository : IUserRepository
  {
    private readonly DataContext _context;

    public UserRepository(DataContext context)
    {
      this._context = context;      
    }

    public async Task<ICollection<User>> GetUsersAsync()
    {
      ICollection<User> users;

      try
      {
        // Todo Products kann von Employee liefert werden. Include(u => u.Products)
        users = await this._context.Users.Include(u => u.UserRole)
          .Include(u => u.Address).ToListAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return users;
    }

    public async Task<User> GetUserByIdAsync(int userId)
    {
      User user;

      try
      {
        // Todo Products kann von Employee liefert werden. Include(u => u.Products)
        user = await this._context.Users.Include(u => u.UserRole)
          .Include(u => u.Address)
          .FirstOrDefaultAsync(u => u.Id == userId);
      }
      catch (Exception e)
      {
        throw e;
      }

      return user;
    }

    public async Task<ICollection<User>> GetUsersByRoleIdAsync(int roleId)
    {
      ICollection<User> users;

      try
      {
        // Todo Products kann von Employee liefert werden. Include(u => u.Products)
        users = await this._context.Users.Include(u => u.UserRole)
          .Include(u => u.Address)
          .Where(u => u.UserRoleId == roleId).ToListAsync();
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        throw;
      }

      return users;
    }
  }
}
