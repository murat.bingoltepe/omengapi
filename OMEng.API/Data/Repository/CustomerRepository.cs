﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.CustomerDtos;
using OMEng.API.Enums;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class CustomerRepository : ICustomerRepository
  {
    private readonly DataContext _context;
    private readonly IAppRepository _appRepository;

    public CustomerRepository(DataContext context, IAppRepository appRepository)
    {
      this._context = context;
      this._appRepository = appRepository;
    }

    public async Task<ICollection<Customer>> GetCustomersAsync(bool include = false)
    {
      ICollection<Customer> customers;

      try
      {
        if (include)
        {
          customers = await this._context.Customers
            .Include(c => c.UserRole)
            .Include(c => c.Address)            
            .Include(c => c.Account)
            .ToListAsync();
        }
        else
        {
          customers = await this._context.Customers.ToListAsync();
        }        
      }
      catch (Exception e)
      {
        throw e;
      }

      return customers;
    }

    public async Task<Customer> GetCustomerByIdAsync(int customerId, bool include = false)
    {
      Customer customer;

      try
      {
        if (include)
        {
          customer = await this._context.Customers
            .Include(c => c.UserRole)
            .Include(c => c.Address)
            .Include(c => c.Account)
            .FirstOrDefaultAsync(c => c.Id == customerId);
        }
        else
        {
          customer = await this._context.Customers.FirstOrDefaultAsync(c => c.Id == customerId);
        }        
      }
      catch (Exception e)
      {
        throw e;
      }

      return customer;
    }
    public async Task<Customer> GetCustomerByUserNameAsync(string userName, bool include = false)
    {
      Customer customer;

      try
      {
        if (include)
        {
          customer = await this._context.Customers
            .Include(c => c.UserRole)
            .Include(c => c.Address)
            .Include(c => c.Account)
            .FirstOrDefaultAsync(c => c.UserName.ToLower() == userName.ToLower());
        }
        else
        {
          customer = await this._context.Customers.FirstOrDefaultAsync(c => c.UserName.ToLower() == userName.ToLower());
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return customer;
    }

    public async Task<Customer> AddAsync(CustomerForAddDto customerForAddDto)
    {
      Customer customer = new Customer();
      _context.Entry(customer).CurrentValues.SetValues(customerForAddDto);

      customer.Address = CheckAddressForAdd(customerForAddDto.Address);
      customer.Account = CheckAccountForAdd(customerForAddDto.Account);
      customer.UserRoleId = (int)UserRoles.Customer;

      customer.TempPassword = true;
      customer.EmailConfirmed = false;

      byte[] passwordHash;
      byte[] passwordSalt;

      CreatePasswordHash(customerForAddDto.Password, out passwordHash, out passwordSalt);

      customer.PasswordHash = passwordHash;
      customer.PasswordSalt = passwordSalt;

      try
      {
        await this._appRepository.AddAsync(customer);
        await this._appRepository.SaveAllAsync();

        return customer;
      }
      catch (Exception e)
      {
        throw e;
      }

    }

    public async Task<Customer> UpdateAsync(CustomerForUpdateDto customerForUpdateDto)
    {
      Customer customer = await GetCustomerByIdAsync(customerForUpdateDto.Id, true);

      _context.Entry(customer).CurrentValues.SetValues(customerForUpdateDto);
      _context.Entry(customer.Address).CurrentValues.SetValues(customerForUpdateDto.Address);
      _context.Entry(customer.Account).CurrentValues.SetValues(customerForUpdateDto.Account);

      // AdressId soll in Address Entity über tragen werden.
      // Sons wird nicht korrekte Update.
      customer.Address.Id = customer.AddressId;

      // AccountId soll in Account Entity über tragen werden.
      // Sons wird nicht korrekte Update.
      customer.Account.Id = customer.AccountId;

      try
      {
        this._appRepository.Update(customer);
        this._appRepository.SaveAll();

        return customer;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public void Delete(Customer customer)
    {
      try
      {
        this._appRepository.Delete(customer);
        this._appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

    }

    public async Task<bool> CustomerExistAsync(string customerName)
    {
      try
      {
        Customer existsCustomer = await this.GetCustomerByUserNameAsync(customerName);

        return existsCustomer != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<bool> CustomerExistAsync(int customerId)
    {
      try
      {
        Customer existsCustomer = await this.GetCustomerByIdAsync(customerId);

        return existsCustomer != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<bool> EmailExistAsync(string email)
    {
      try
      {
        Customer existsEmail = await _context.Customers.FirstOrDefaultAsync(c => c.Email.ToLower() == email.ToLower());

        return existsEmail != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
    {
      using (HMACSHA512 hmac = new HMACSHA512())
      {
        passwordSalt = hmac.Key;
        passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
      }
    }

    private Address CheckAddressForAdd(Address address)
    {
      Address resultAddress = address;
      if (address == null)
      {
        resultAddress = new Address
        {
          City = string.Empty,
          Country = string.Empty,
          Number = string.Empty,
          PostalCode = string.Empty,
          Street = string.Empty
        };
      }

      return resultAddress;
    }

    private Account CheckAccountForAdd(Account account)
    {
      Account resultAccount = account;
      if (account == null)
      {
        resultAccount = new Account
        {
          AccountNumber = string.Empty,
          BankCode = string.Empty,
          BankName = string.Empty,
          Iban = string.Empty,
          Swift = string.Empty
        };
      }

      return resultAccount;
    }    
  }
}
