﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class PhotoRepository : IPhotoRepository
  {
    private readonly DataContext _context;
    private readonly IAppRepository _appRepository;

    public PhotoRepository(DataContext context, IAppRepository appRepository)
    {
      this._context = context;
      this._appRepository = appRepository;
    }

    public async Task<ICollection<Photo>> GetPhotosAsync()
    {
      ICollection<Photo> photos;

      try
      {
        photos = await this._context.Photos.Include(p => p.Employee)
          .Include(p => p.Product).ToListAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return photos;
    }
    public async Task<Photo> GetPhotoByIdAsync(int photoId)
    {
      Photo photo;

      try
      {
        photo = await this._context.Photos.Include(p => p.Employee)
          .Include(p => p.Product)
          .FirstOrDefaultAsync(p => p.Id == photoId);
      }
      catch (Exception e)
      {
        throw e;
      }

      return photo;
    }
    public async Task<ICollection<Photo>> GetPhotosByEmployeeIdAsync(int employeeId)
    {
      ICollection<Photo> photos;

      try
      {
        photos = await this._context.Photos.Include(p => p.Employee)         
          .Where(p => p.EmployeeId == employeeId).ToListAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return photos;
    }

    public async Task<ICollection<Photo>> GetPhotosWithDetailByEmployeeIdAsync(int employeeId)
    {
      ICollection<Photo> photos;

      try
      {
        photos = await this._context.Photos.Include(p => p.Employee)
          .Include(p => p.Product)
          .Where(p => p.EmployeeId == employeeId).ToListAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return photos;
    }

    public async Task<ICollection<Photo>> GetPhotosByProductIdAsync(int productId)
    {
      ICollection<Photo> photos;

      try
      {
        photos = await this._context.Photos.Include(p => p.Employee)
          .Include(p => p.Product)
          .Where(p => p.ProductId == productId).ToListAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return photos;
    }

    public Photo Update(Photo photo)
    {
      try
      {
        this._appRepository.Update(photo);
        this._appRepository.SaveAll();

        return photo;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public void UpdateRange(ICollection<Photo> photos)
    {
      try
      {
        this._appRepository.UpdateRange(photos);
        this._appRepository.SaveAllAsync();      
      }
      catch (Exception e)
      {
        throw e;
      }
    }
  }
}
