﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.CategoryDtos;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
	public class CategoryRepository : ICategoryRepository
	{
		private readonly DataContext _context;
		private readonly IAppRepository _appRepository;

		public CategoryRepository(DataContext context, IAppRepository appRepository)
		{
			_context = context;
			_appRepository = appRepository;
		}

		public async Task<ICollection<Category>> GetCategoriesAsync(bool include = false)
		{
			ICollection<Category> categories;

			try
			{
				if (include)
				{
					categories = await _context.Categories.ToListAsync();
				}
				else
				{
					categories = await _context.Categories.ToListAsync();
				}
			}
			catch (Exception e)
			{
				throw e;
			}

			return categories;
		}

		public async Task<ICollection<Category>> GetMainParentsAsync(bool include = false)
		{
			ICollection<Category> categories;

			try
			{
				if (include)
				{
					categories = await _context.Categories.Where(c => c.ParentId == 0).ToListAsync();
				}
				else
				{
					categories = await _context.Categories.Where(c => c.ParentId == 0).ToListAsync();
				}
			}
			catch (Exception e)
			{
				throw e;
			}

			return categories;
		}

		public async Task<Category> GetCategoryByIdAsync(int categoryId, bool include = false)
		{
			Category category;

			try
			{
				if (include)
				{
					category = await _context.Categories.FirstOrDefaultAsync(c => c.Id == categoryId);
				}
				else
				{
					category = await _context.Categories.FirstOrDefaultAsync(c => c.Id == categoryId);
				}
			}
			catch (Exception e)
			{
				throw e;
			}

			return category;
		}

		public async Task<ICollection<Category>> GetCategoriesByParentIdAsync(int parentId, bool include = false)
		{
			ICollection<Category> categories;

			try
			{
				if (include)
				{
					categories = await _context.Categories.Where(c => c.ParentId == parentId).ToListAsync();
				}
				else
				{
					categories = await _context.Categories.Where(c => c.ParentId == parentId).ToListAsync();
				}
			}
			catch (Exception e)
			{
				throw e;
			}

			return categories;
		}

		public async Task<Category> AddAsync(CategoryForAddDto categoryForAddDto)
		{
			Category category = new Category();
			_context.Entry(category).CurrentValues.SetValues(categoryForAddDto);

			try
			{
				await _appRepository.AddAsync(category);
				await _appRepository.SaveAllAsync();

				return category;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void AddRange(ICollection<CategoryForAddDto> categories)
		{
			ICollection<Category> categoryList = new List<Category>();

			foreach (CategoryForAddDto categoryForAddDto in categories)
			{
				Category category = new Category();
				_context.Entry(category).CurrentValues.SetValues(categoryForAddDto);
				categoryList.Add(category);
			}

			try
			{
				this._appRepository.AddRange(categoryList);
				this._appRepository.SaveAll();
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public async Task<Category> UpdateAsync(CategoryForUpdateDto categoryForUpdateDto)
		{
			Category category = await GetCategoryByIdAsync(categoryForUpdateDto.Id, true);
			_context.Entry(category).CurrentValues.SetValues(categoryForUpdateDto);

			try
			{
				_appRepository.Update(category);
				_appRepository.SaveAll();

				return category;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public async void UpdateRangeAsync(ICollection<CategoryForUpdateDto> categories)
		{
			ICollection<Category> categoryList = new List<Category>();

			foreach (CategoryForUpdateDto categoryForUpdateDto in categories)
			{
				Category category = await GetCategoryByIdAsync(categoryForUpdateDto.Id, true);
				_context.Entry(category).CurrentValues.SetValues(categoryForUpdateDto);
				categoryList.Add(category);
			}

			try
			{
				this._appRepository.UpdateRange(categoryList);
				this._appRepository.SaveAll();
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Delete(Category category)
		{
			try
			{
				_appRepository.Delete(category);
				_appRepository.SaveAll();
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void DeleteRange(ICollection<Category> categories)
		{
			try
			{
				_appRepository.DeleteRange(categories);
				_appRepository.SaveAll();
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public async Task<bool> CategoryExistAsync(int categoryId)
		{
			try
			{
				Category existsCategory = await GetCategoryByIdAsync(categoryId);

				return existsCategory != null;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}
