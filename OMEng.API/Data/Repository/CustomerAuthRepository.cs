﻿using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Models;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class CustomerAuthRepository : ICustomerAuthRepository
  {
    private readonly ICustomerRepository _customerRepository;
    private readonly IAppRepository _appRepository;

    public CustomerAuthRepository(ICustomerRepository customerRepository, IAppRepository appRepository)
    {
      this._customerRepository = customerRepository;
      this._appRepository = appRepository;      
    }

    public async Task<Customer> Login(string userName, string password)
    {
      try
      {
        Customer customer = await this._customerRepository.GetCustomerByUserNameAsync(userName);

        if (customer != null)
        {
          if (!VerifyPasswordHash(password, customer.PasswordHash, customer.PasswordSalt))
          {
            return null;
          }
        }

        return customer;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<Customer> Register(Customer customer, string password)
    {
      byte[] passwordHash;
      byte[] passwordSalt;

      CreatePasswordHash(password, out passwordHash, out passwordSalt);

      customer.PasswordHash = passwordHash;
      customer.PasswordSalt = passwordSalt;

      try
      {
        await this._appRepository.AddAsync(customer);
        await this._appRepository.SaveAllAsync();

        return customer;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<Customer> ChangePassword(Customer customer)
    {
      var customerToUpdate = await this._customerRepository.GetCustomerByUserNameAsync(customer.UserName);

      byte[] passwordHash;
      byte[] passwordSalt;

      CreatePasswordHash(customerToUpdate.UserName, out passwordHash, out passwordSalt);

      customerToUpdate.PasswordHash = passwordHash;
      customerToUpdate.PasswordSalt = passwordSalt;

      try
      {
        this._appRepository.Update(customerToUpdate);
        this._appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return customerToUpdate;
    }

    public async Task<bool> UserExist(string userName)
    {
      try
      {
        Customer existsUser = await this._customerRepository.GetCustomerByUserNameAsync(userName);

        return existsUser != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    private bool VerifyPasswordHash(string password, byte[] userPasswordHash, byte[] userPasswordSalt)
    {
      using (HMACSHA512 hmac = new HMACSHA512(userPasswordSalt))
      {
        byte[] computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

        for (int i = 0; i < computedHash.Length; i++)
        {
          if (computedHash[i] != userPasswordHash[i])
            return false;
        }

        return true;
      }
    }

    private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
    {
      using (HMACSHA512 hmac = new HMACSHA512())
      {
        passwordSalt = hmac.Key;
        passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
      }
    }
  }
}
