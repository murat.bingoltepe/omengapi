﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.OrderDetailDtos;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
	public class OrderDetailRepository : IOrderDetailRepository
	{
		private readonly DataContext _context;
		private readonly IAppRepository _appRepository;

		public OrderDetailRepository(DataContext context, IAppRepository appRepository)
		{
			_context = context;
			_appRepository = appRepository;
		}

		#region GetOrderDetailsAsync(bool include = false)
		/// <summary>
		/// Alle Details von aller Bestellung werden zurück gelifiert.
		/// </summary>
		/// <param name="include">Legt fest ob zu gehörige Objekten 
		/// mit gelifert werden soll.</param>
		/// <returns>Gefundene Details von aller Vestellungen.</returns>
		public async Task<ICollection<OrderDetail>> GetOrderDetailsAsync(bool include = false)
		{
			ICollection<OrderDetail> orderDetails;

			try
			{
				if (include)
				{
					orderDetails = await this._context.OrderDetails
						.Include(o => o.Order)
						.Include(o => o.Product)
						.ToListAsync();
				}
				else
				{
					orderDetails = await this._context.OrderDetails.ToListAsync();
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}

			return orderDetails;
		}
		#endregion

		#region GetOrderDetails(bool include = false)
		/// <summary>
		/// Alle Details von aller Bestellung werden zurück gelifiert.
		/// </summary>
		/// <param name="include">Legt fest ob zu gehörige Objekten 
		/// mit gelifert werden soll.</param>
		/// <returns>Gefundene Details von aller Vestellungen.</returns>
		public ICollection<OrderDetail> GetOrderDetails(bool include = false)
		{
			ICollection<OrderDetail> orderDetails;

			try
			{
				if (include)
				{
					orderDetails = this._context.OrderDetails
						.Include(o => o.Order)
						.Include(o => o.Product)
						.ToList();
				}
				else
				{
					orderDetails = this._context.OrderDetails.ToList();
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}

			return orderDetails;
		}
		#endregion
			
		#region GetOrderDetailById(int orderDetailId, bool include = false)
		/// <summary>
		/// Alle Details von aller Bestellung werden zurück gelifiert.
		/// </summary>
		/// <param name="include">Legt fest ob zu gehörige Objekten 
		/// mit gelifert werden soll.</param>
		/// <returns>Gefundene Details von aller Vestellungen.</returns>

		public OrderDetail GetOrderDetailById(int orderDetailId, bool include = false)
		{
			throw new NotImplementedException();
		}
		#endregion

		public Task<bool> AddAsync(OrderDetailForAddDto orderDetailForAddDto)
		{
			throw new NotImplementedException();
		}		

		public Task<OrderDetail> GetOrderDetailByIdAsync(int orderDetailId, bool include = false)
		{
			throw new NotImplementedException();
		}		

		public Task<ICollection<OrderDetail>> GetOrderDetailsByOrderIdAsync(int orderId, bool include = false)
		{
			throw new NotImplementedException();
		}

		public Task<ICollection<OrderDetail>> GetOrderDetailsByProductIdAsync(int productId, bool include = false)
		{
			throw new NotImplementedException();
		}

		public bool Update(OrderDetailForUpdate orderDetailForUpdate)
		{
			throw new NotImplementedException();
		}
	}
}
