﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class AccountRepository : IAccountRepository
  {
    private readonly DataContext _context;
    private readonly IAppRepository _appRepository;

    public AccountRepository(DataContext context, IAppRepository appRepository)
    {
      this._context = context;
      this._appRepository = appRepository;
    }
  
    public async Task<ICollection<Account>> GetAccountsAsync()
    {
      ICollection<Account> accounts;

      try
      {
        accounts = await this._context.Accounts.ToListAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return accounts;
    }

    public async Task<Account> GetAccountsByIdAsync(int accountId)
    {
      Account account;

      try
      {
        account = await this._context.Accounts.FirstOrDefaultAsync(a => a.Id == accountId);
      }
      catch (Exception e)
      {
        throw e;
      }

      return account;
    }

    public void Delete(Account account)
    {

      try
      {
        this._appRepository.Delete(account);
        this._appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }
    }
  }
}
