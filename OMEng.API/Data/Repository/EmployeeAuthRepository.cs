﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Models;

namespace OMEng.API.Data.Repository
{
  public class EmployeeAuthRepository : IEmployeeAuthRepository
  {
    private readonly IEmployeeRepository _employeeRepository;
    private readonly IAppRepository _appRepository;

    public EmployeeAuthRepository(IEmployeeRepository employeeRepository, IAppRepository appRepository)
    {
      this._employeeRepository = employeeRepository;
      this._appRepository = appRepository;
    }

    public async Task<Employee> Login(string userName, string password)
    {
      try
      {
        Employee employee = await this._employeeRepository.GetEmployeeByUserNameAsync(userName);

        if (employee != null)
        {
          if (!VerifyPasswordHash(password, employee.PasswordHash, employee.PasswordSalt))
          {
            return null;
          }
        }

        return employee;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<Employee> Register(Employee employee, string password)
    {
      byte[] passwordHash;
      byte[] passwordSalt;

      CreatePasswordHash(password, out passwordHash, out passwordSalt);

      employee.PasswordHash = passwordHash;
      employee.PasswordSalt = passwordSalt;

      try
      {
        await this._appRepository.AddAsync(employee);
        await this._appRepository.SaveAllAsync();

        return employee;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public async Task<Employee> ChangePassword(Employee employee)
    {
      var employeeToUpdate = await this._employeeRepository.GetEmployeeByUserNameAsync(employee.UserName);

      byte[] passwordHash;
      byte[] passwordSalt;

      CreatePasswordHash(employeeToUpdate.UserName, out passwordHash, out passwordSalt);

      employeeToUpdate.PasswordHash = passwordHash;
      employeeToUpdate.PasswordSalt = passwordSalt;

      try
      {
        this._appRepository.Update(employeeToUpdate);
        this._appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return employeeToUpdate;
    }

    public async Task<bool> UserExist(string userName)
    {
      try
      {
        Employee existsUser = await this._employeeRepository.GetEmployeeByUserNameAsync(userName);

        return existsUser != null;
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    private bool VerifyPasswordHash(string password, byte[] userPasswordHash, byte[] userPasswordSalt)
    {
      using (HMACSHA512 hmac = new HMACSHA512(userPasswordSalt))
      {
        byte[] computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

        for (int i = 0; i < computedHash.Length; i++)
        {
          if (computedHash[i] != userPasswordHash[i])
            return false;
        }

        return true;
      }
    }

    private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
    {
      using (HMACSHA512 hmac = new HMACSHA512())
      {
        passwordSalt = hmac.Key;
        passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
      }
    }
  }
}
