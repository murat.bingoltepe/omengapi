﻿// Copyright (c) Omeng (2021). All Rights Reserved.

using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.OrderDtos;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
	public class OrderRepository : IOrderRepository
	{
		private readonly DataContext _context;
		private readonly IAppRepository _appRepository;

		public OrderRepository(DataContext context, IAppRepository appRepository)
		{
			_context = context;
			_appRepository = appRepository;
		}

		#region GetOrdersAsync(bool include = false)
		/// <summary>
		/// Alle Bestellungen werden zurück gelifiert.
		/// </summary>
		/// <param name="include">Legt fest ob zu gehörige Objekten 
		/// mit gelifert werden soll.</param>
		/// <returns>Gefundene Bestellungen.</returns>
		public async Task<ICollection<Order>> GetOrdersAsync(bool include = false)
		{
			ICollection<Order> orders;

			try
			{
				if (include)
				{
					orders = await this._context.Orders
						.Include(o => o.Customer)
						.Include(o => o.OrderDetails)
						.ToListAsync();
				}
				else
				{
					orders = await this._context.Orders.ToListAsync();
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}

			return orders;
		}
		#endregion

		#region GetOrders(bool include = false)
		/// <summary>
		/// Alle Bestellungen werden zurück gelifiert.
		/// </summary>
		/// <param name="include">Legt fest ob zu gehörige Objekten 
		/// mit gelifert werden soll.</param>
		/// <returns>Gefundene Bestellungen.</returns>
		public ICollection<Order> GetOrders(bool include = false)
		{
			ICollection<Order> orders;

			try
			{
				if (include)
				{
					orders = this._context.Orders
						.Include(o => o.Customer)
						.Include(o => o.OrderDetails)
						.ToList();
				}
				else
				{
					orders = this._context.Orders.ToList();
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}

			return orders;
		}
		#endregion

		#region GetOrderByIdAsync(int orderId, bool include = false)
		/// <summary>
		/// Durch Bestellung Id wird die Bestellung zurück gelifiert.
		/// </summary>
		/// <param name="orderId">Bestellung Id</param>
		/// <param name="include">Legt fest ob zu gehörige Objekten 
		/// mit gelifert werden soll.</param>
		/// <returns>Gefundene Bestellung.</returns>
		public async Task<Order> GetOrderByIdAsync(int orderId, bool include = false)
		{
			Order order;

			try
			{
				if (include)
				{
					order = await this._context.Orders
						.Include(o => o.Customer)
						.Include(o => o.OrderDetails)
						.FirstOrDefaultAsync(o => o.Id == orderId);
				}
				else
				{
					order = await this._context.Orders.FirstOrDefaultAsync(o => o.Id == orderId);
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}

			return order;
		}
		#endregion

		#region GetOrderById(int orderId, bool include = false)
		/// <summary>
		/// Durch Bestellung Id wird die Bestellung zurück gelifiert.
		/// </summary>
		/// <param name="orderId">Bestellung Id</param>
		/// <param name="include">Legt fest ob zu gehörige Objekten 
		/// mit gelifert werden soll.</param>
		/// <returns>Gefundene Bestellungen.</returns>
		public Order GetOrderById(int orderId, bool include = false)
		{
			Order order;

			try
			{
				if (include)
				{
					order = this._context.Orders
						.Include(o => o.Customer)
						.Include(o => o.OrderDetails)
						.FirstOrDefault(o => o.Id == orderId);
				}
				else
				{
					order = this._context.Orders.FirstOrDefault(o => o.Id == orderId);
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}

			return order;
		}
		#endregion

		#region GetOrdersByCustomerIdAsync(int customerId, bool include = false)
		/// <summary>
		/// Durch Kunde Id werden alle Bestellungen zurück gelifiert.		
		/// </summary>
		/// <param name="customerId">Kunde Id</param>
		/// <param name="include">Legt fest ob zu gehörige Objekten 
		/// mit gelifert werden soll.</param>
		/// <returns>Gefundene Bestellungen.</returns>
		public async Task<ICollection<Order>> GetOrdersByCustomerIdAsync(int customerId, bool include = false)
		{
			ICollection<Order> orders;

			try
			{
				if (include)
				{
					orders = await this._context.Orders
						.Include(o => o.Customer)
						.Include(o => o.OrderDetails)
						.Where(o => o.CustomerId == customerId).ToListAsync();
				}
				else
				{
					orders = await this._context.Orders.Where(o => o.CustomerId == customerId).ToListAsync();
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}

			return orders;
		}
		#endregion

		#region AddAsync(OrderForAddDto orderForAddDto)
		/// <summary>
		/// Bestellung wird in der DatenBank gespeichert.
		/// </summary>
		/// <param name="orderForAddDto">Die Bestellung, die in der DatenBank
		/// gespeichert wird.</param>
		/// <returns></returns>
		public async Task<bool> AddAsync(OrderForAddDto orderForAddDto)
		{
			Order order = new Order();
			this._context.Entry(order).CurrentValues.SetValues(orderForAddDto);

			try
			{
				await this._appRepository.AddAsync(order);
				await this._appRepository.SaveAllAsync();

				return true;
			}
			catch (Exception exception)
			{
				throw exception;
			}

		}
		#endregion

		#region Update(OrderForAddDto orderForAddDto)
		/// <summary>
		/// Bestellung wird in der DatenBank aktualisiert.
		/// </summary>
		/// <param name="orderForAddDto">Die Bestellung, die in der DatenBank
		/// aktualisiert wird.</param>
		/// <returns></returns>
		public bool Update(OrderForUpdateDto orderForUpdateDto)
		{
			Order order = GetOrderById(orderForUpdateDto.Id, true);
			_context.Entry(order).CurrentValues.SetValues(orderForUpdateDto);

			// Beim Update Customer Entity darf nicht mit gegeben werden.
			// Es genug wen mann nun die EmployeeId mit schickt.
			order.Customer = null;

			try
			{
				this._appRepository.Update(order);
				this._appRepository.SaveAll();

				return true;
			}
			catch (Exception exception)
			{

				throw exception;
			}
		}
		#endregion

		#region Delete(Order order)
		/// <summary>
		/// Bestellung wird aus der DatenBank gelöscht.
		/// </summary>
		/// <param name="order">Die Bestellung, die aus der DatenBank
		/// gelöscht wird.</param>
		/// <returns></returns>
		public bool Delete(Order order)
		{
			try
			{
				this._appRepository.Delete(order);
				this._appRepository.SaveAll();

				return true;
			}
			catch (Exception exception)
			{

				throw exception;
			}
		}
		#endregion
	}
}
