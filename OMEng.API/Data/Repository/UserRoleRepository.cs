﻿using Microsoft.EntityFrameworkCore;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Data.Repository
{
  public class UserRoleRepository : IUserRoleRepository
  {
    private readonly DataContext _context;

    public UserRoleRepository(DataContext context)
    {
      this._context = context;    
    }

    public async Task<ICollection<UserRole>> GetUserRolesAsync()
    {
      ICollection<UserRole> userRoles;

      try
      {
        // Todo Users kann von User liefert werden. Include(u => u.Users)
        userRoles = await this._context.UserRoles.ToListAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return userRoles;
    }

    public async Task<UserRole> GetUserRoleByIdAsync(int userId)
    {
      UserRole userRole;

      try
      {
        // Todo Users kann von User liefert werden. Include(u => u.Users)
        userRole = await this._context.UserRoles.FirstOrDefaultAsync(u => u.Id == userId);
      }
      catch (Exception e)
      {
        throw e;
      }

      return userRole;
    }
  }
}
