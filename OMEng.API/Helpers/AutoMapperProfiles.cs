﻿using AutoMapper;
using OMEng.API.Dtos;
using OMEng.API.Dtos.CategoryDtos;
using OMEng.API.Dtos.CustomerDtos;
using OMEng.API.Dtos.EmployeeDtos;
using OMEng.API.Dtos.ProductDtos;
using OMEng.API.Models;
using System.Linq;

namespace OMEng.API.Helpers
{
  public class AutoMapperProfiles : Profile
  {
    public AutoMapperProfiles()
    {
     
      // Address
      CreateMap<Address, AddressDto>();

      // Employee
      CreateMap<Employee, EmployeeForListDto>();
      CreateMap<Employee, EmployeeForDetailDto>();
      CreateMap<EmployeeForAddDto, Employee>();
      CreateMap<EmployeeForUpdateDto, Employee>();

      // Customer
      CreateMap<Customer, CustomerForListDto>();
      CreateMap<Customer, CustomerForDetailDto>();
      CreateMap<CustomerForAddDto, Customer>();
      CreateMap<CustomerForUpdateDto, Customer>();

      // Product
      CreateMap<Product, ProductForListDto>()
        .ForMember(dest => dest.PhotoUrl, opt =>
        {
          opt.MapFrom(src => src.Photos.ToList().FirstOrDefault(p => p.IsMain).Path); 
        });
      CreateMap<Product, ProductForDetailDto>();
      CreateMap<ProductForAddDto, Product>();
      CreateMap<ProductForUpdateDto, Product>();      

      // Photo
      CreateMap<Photo, PhotoDto>();

      // Category
      CreateMap<Category, CategoryForListDto>();
      CreateMap<Category, CategoryForDetailDto>();
      CreateMap<CategoryForAddDto, Category>();
      CreateMap<CategoryForUpdateDto, Category>();

      // User
      CreateMap<User, UserDto>();

      // UserRole
      CreateMap<UserRole, UserRoleDto>();
    }
  }
}
