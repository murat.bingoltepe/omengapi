﻿using Microsoft.AspNetCore.Http;

namespace OMEng.API.Helpers
{
  /**
   * JWT (Json Web Tokens)
   */
  public static class JwtExtension
  {
    public static void AddApplicationError(this HttpResponse response, string message)
    {
      response.Headers.Add("Application-Error", message);
      response.Headers.Add("Access-Control-Allow-Origin", "*"); // Sadece bizim sayfadan gelenler erisebilmeli
      response.Headers.Add("Access-Control-Expose-Header", "Application-Error");
    }
  }
}
