﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos;
using OMEng.API.Models;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class PhotosController : ControllerBase
  {
    private IAppRepository _appRepository;
    private IPhotoRepository _photoRepository;
    private IMapper _mapper;

    public PhotosController(IAppRepository appRepository, IPhotoRepository photoRepository, IMapper mapper)
    {
      _appRepository = appRepository;
      _photoRepository = photoRepository;
      _mapper = mapper;
    }

    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetPhotosAsync()
    {
      ICollection<Photo> photos;

      try
      {
        photos = await _photoRepository.GetPhotosAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      ICollection<PhotoDto> photosToReturn = _mapper.Map<ICollection<PhotoDto>>(photos);
      return Ok(photosToReturn);
    }

    [Route("detail/ById")]
    public async Task<IActionResult> GetPhotoByIdAsync(int id)
    {
      Photo photo;

      try
      {
        photo = await _photoRepository.GetPhotoByIdAsync(id);
      }
      catch (Exception e)
      {
        throw e;
      }

      PhotoDto photoToReturn = _mapper.Map<PhotoDto>(photo);
      return Ok(photoToReturn);
    }

    [Route("detail/ByEId")]
    public async Task<ActionResult> GetPhotosByEmployeeIdAsync(int id)
    {
      ICollection<Photo> photos;
      try
      {
        photos = await _photoRepository.GetPhotosByEmployeeIdAsync(id);
      }
      catch (Exception e)
      {
        throw e;
      }

      ICollection<PhotoDto> photosToReturn = _mapper.Map<ICollection<PhotoDto>>(photos);
      return Ok(photosToReturn);
    }

    [Route("detail/ByPId")]
    public async Task<ActionResult> GetPhotosByProductIdAsync(int id)
    {
      ICollection<Photo> photos;

      try
      {
        photos = await _photoRepository.GetPhotosByProductIdAsync(id);
      }
      catch (Exception e)
      {
        throw e;
      }

      ICollection<PhotoDto> photosToReturn = _mapper.Map<ICollection<PhotoDto>>(photos);
      return Ok(photosToReturn);
    }

    [HttpPost]
    [Route("add")]
    public async Task<IActionResult> AddAsync([FromBody] Photo photo)
    {
      try
      {
        await _appRepository.AddAsync(photo);
        await _appRepository.SaveAllAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok(photo);
    }

    [HttpPost]
    [Route("update")]
    public ActionResult Update([FromBody] Photo photo)
    {
      try
      {
        _appRepository.Update(photo);
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok(photo);
    }

    [HttpDelete]
    [Route("delete")]
    public ActionResult Delete([FromBody] Photo photo)
    {
      try
      {
        _appRepository.Delete(photo);
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok();
    }
  }
}