﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OMEng.API.Dtos.ProductDtos;
using OMEng.API.Handler.ErrorHandler.Implements;
using OMEng.API.Models;
using OMEng.API.Services.Interfaces;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class ProductsController : ControllerBase
  {
    private readonly IBusinessManagment _businessManagment;
    private readonly IMapper _mapper;

    public ProductsController(IBusinessManagment businessManagment, IMapper mapper)
    {
      _businessManagment = businessManagment;
      _mapper = mapper;
    }

    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetProductsAsync()
    {
      ICollection<Product> products;

      try
      {
        products = await _businessManagment.GetProductsAsync(true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ICollection<ProductForListDto> productsToReturn = _mapper.Map<ICollection<ProductForListDto>>(products);
      return Ok(productsToReturn);
    }

    [Route("detail/ByPId")]
    public async Task<IActionResult> GetProductByIdAsync(int id)
    {
      Product product = new Product
      {
        Id = id
      };

      Dictionary<string, string> results = await ErrorHandler.CheckProductForByIdAsync(product);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        product = await _businessManagment.GetProductByIdAsync(id, true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ProductForDetailDto productToReturn = _mapper.Map<ProductForDetailDto>(product);
      return Ok(productToReturn);
    }

    [Route("detail/ByEId")]
    public async Task<IActionResult> GetProducstByEmployeeIdAsync(int id)
    {
      Product product = new Product
      {
        EmployeeId = id
      };

      Dictionary<string, string> results = await ErrorHandler.CheckProductForByEmployeeIdAsync(product);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      ICollection<Product> products;
      try
      {
        products = await _businessManagment.GetProductsByEmployeeIdAsync(id, true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ICollection<ProductForListDto> productsToReturn = _mapper.Map<ICollection<ProductForListDto>>(products);
      return Ok(productsToReturn);
    }

    [Route("detail/ByCId")]
    public async Task<IActionResult> GetProducstByCategoryIdAsync(int id)
    {
      Product product = new Product
      {
        CategoryId = id
      };

      Dictionary<string, string> results = await ErrorHandler.CheckProductForByCategoryIdAsync(product);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      ICollection<Product> products;
      try
      {
        products = await _businessManagment.GetProductsByCategoryIdAsync(id, true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ICollection<ProductForListDto> productsToReturn = _mapper.Map<ICollection<ProductForListDto>>(products);
      return Ok(productsToReturn);
    }

    [HttpPost]
    [Route("add")]
    public async Task<IActionResult> AddAsync([FromBody] ProductForAddDto productForAddDto)
    {
      Product product = _mapper.Map<Product>(productForAddDto);
      Dictionary<string, string> results = await ErrorHandler.CheckProductForAddAsync(product);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.AddAsync(productForAddDto);        
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(201);
    }

    [HttpPost]
    [Route("update")]
    public async Task<IActionResult> UpdateAsync([FromBody] ProductForUpdateDto productForUpdateDto)
    {
      Product product = _mapper.Map<Product>(productForUpdateDto);
      Dictionary<string, string> results = await ErrorHandler.CheckProductForUpdateAsync(product);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.UpdateAsync(productForUpdateDto);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(200);
    }

    [HttpDelete]
    [Route("delete/ByPId")]
    public async Task<IActionResult> DeleteByIdAsync(int id)
    {
      Product product = await _businessManagment.GetProductByIdAsync(id);
      if (product == null)
      {
        product = new Product
        {
          Id = id
        };
      }

      Dictionary<string, string> results = await ErrorHandler.CheckProductForDeleteAsync(product);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.DeleteProductByIdAsync(id);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return Ok();
    }

    protected ModelStateDictionary PrepareModelStateForErrors(Dictionary<string, string> errors)
    {
      if (errors.Count != 0)
      {
        foreach (KeyValuePair<string, string> error in errors)
        {
          ModelState.AddModelError(error.Key, error.Value);
        }
        return ModelState;
      }

      return null;
    }
  }
}