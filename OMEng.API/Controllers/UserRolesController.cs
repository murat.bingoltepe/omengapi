﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos;
using OMEng.API.Models;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class UserRolesController : ControllerBase
  {
    private IAppRepository _appRepository;
    private IUserRoleRepository _userRoleRepository;
    private IMapper _mapper;

    public UserRolesController(IAppRepository appRepository, IUserRoleRepository userRoleRepository, IMapper mapper)
    {
      _appRepository = appRepository;
      _userRoleRepository = userRoleRepository;
      _mapper = mapper;
    }

    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetUserRolesAsync()
    {
      ICollection<UserRole> userRoles;

      try
      {
        userRoles = await _userRoleRepository.GetUserRolesAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      ICollection<UserRoleDto> userRolesToReturn = _mapper.Map<ICollection<UserRoleDto>>(userRoles);
      return Ok(userRolesToReturn);
    }

    [HttpGet]
    [Route("detail/ByRId")]
    public async Task<ActionResult> GetUserRoleByIdAsync(int id)
    {
      UserRole userRole;

      try
      {
        userRole = await _userRoleRepository.GetUserRoleByIdAsync(id);
      }
      catch (Exception e)
      {
        throw e;
      }

      UserRoleDto userRoleToReturn = _mapper.Map<UserRoleDto>(userRole);
      return Ok(userRoleToReturn);
    }

    [HttpPost]
    [Route("add")]
    public async Task<IActionResult> AddAsync([FromBody] UserRole userRole)
    {
      try
      {
        await _appRepository.AddAsync(userRole);
        await _appRepository.SaveAllAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok(userRole);
    }

    [HttpPost]
    [Route("update")]
    public ActionResult Update([FromBody] UserRole userRole)
    {
      try
      {
        _appRepository.Update(userRole);
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok(userRole);
    }

    [HttpDelete]
    [Route("delete")]
    public ActionResult Delete([FromBody] UserRole userRole)
    {
      try
      {
        _appRepository.Delete(userRole);
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok();
    }
  }
}