﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos;
using OMEng.API.Models;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class AddressesController : ControllerBase
  {
    private IAppRepository _appRepository;
    private IAddressRepository _addressRepository;
    private IMapper _mapper;

    public AddressesController(IAppRepository appRepository, IAddressRepository addressRepository, IMapper mapper)
    {
      _appRepository = appRepository;
      _addressRepository = addressRepository;
      _mapper = mapper;
    }

    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetAddressesAsync()
    {
      ICollection<Address> addresses;

      try
      {
        addresses = await _addressRepository.GetAddressesAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      ICollection<AddressDto> addressesToReturn = _mapper.Map<ICollection<AddressDto>>(addresses);
      return Ok(addressesToReturn);
    }

    [HttpGet]
    [Route("detail/ByAId")]
    public async Task<IActionResult> GetAddressByIdAsync(int id)
    {
      Address address;

      try
      {
        address = await _addressRepository.GetAddressByIdAsync(id);
      }
      catch (Exception e)
      {
        throw e;
      }

      AddressDto addressToReturn = _mapper.Map<AddressDto>(address);
      return Ok(addressToReturn);
    }

    [HttpPost]
    [Route("add")]
    public async Task<IActionResult> AddAsync([FromBody] Address address)
    {
      try
      {
        await _appRepository.AddAsync(address);
        await _appRepository.SaveAllAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok(address);
    }

    [HttpPost]
    [Route("update")]
    public ActionResult Update([FromBody] Address address)
    {
      try
      {
        _appRepository.Update(address);
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok(address);
    }

    [HttpDelete]
    [Route("delete")]
    public ActionResult Delete([FromBody] Address address)
    {
      try
      {
        _appRepository.Delete(address);
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok();
    }
  }
}