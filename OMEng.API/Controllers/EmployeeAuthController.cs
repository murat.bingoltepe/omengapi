﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.EmployeeDtos;
using OMEng.API.Models;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class EmployeeAuthController : ControllerBase
  {
    private readonly IEmployeeAuthRepository _employeeAuthRepository;
    private readonly IConfiguration _configuration;
    public EmployeeAuthController(IEmployeeAuthRepository employeeAuthRepository, IConfiguration configuration)
    {
      _employeeAuthRepository = employeeAuthRepository;
      _configuration = configuration;
    }

  
    [HttpPost]
    [Route("login")]
    public async Task<ActionResult> Login([FromBody] EmployeeForLoginDto employeeForLoginDto)
    {
      Employee employee = await _employeeAuthRepository.Login(employeeForLoginDto.UserName, employeeForLoginDto.Password);

      if (employee == null)
      {
        return Unauthorized();
      }

      return Ok(GetSecurityToken(employee));
    }

    [HttpPost]
    [Route("changePswd")]
    public async Task<ActionResult> ChangePassword([FromBody] EmployeeForLoginDto customer)
    {
      Employee employeeToUpdate = new Employee
      {
        UserName = customer.UserName
      };

      Employee updatedEmployee = await _employeeAuthRepository.ChangePassword(employeeToUpdate);

      return Ok(updatedEmployee);
    }

    private string GetSecurityToken(Employee employee)
    {
      byte[] key = Encoding.ASCII.GetBytes(_configuration.GetSection("AppSettings:Token").Value);
      SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(new Claim[]
        {
          new Claim(ClaimTypes.NameIdentifier, employee.Id.ToString())
          //new Claim(ClaimTypes.Name, employee.UserName)
        }),
        //Kullanici ne kadar sistemde kalabilir
        Expires = DateTime.Now.AddDays(1),
        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
      };

      // Tokenler login olan kullanicinin haklari ile alakali bilgileri tutar
      JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

      SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
      string tokenString = tokenHandler.WriteToken(token);

      return tokenString;
    }
  }
}