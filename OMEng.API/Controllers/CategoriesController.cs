﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OMEng.API.Dtos.CategoryDtos;
using OMEng.API.Handler.ErrorHandler.Implements;
using OMEng.API.Models;
using OMEng.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class CategoriesController : ControllerBase
  {
    private readonly IBusinessManagment _businessManagment;
    private readonly IMapper _mapper;

    public CategoriesController(IBusinessManagment businessManagment, IMapper mapper)
    {
      _businessManagment = businessManagment;
      _mapper = mapper;
    }
    
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetCategoriesAsync()
    {
      ICollection<Category> categories;

      try
      {
        categories = await _businessManagment.GetMainParentsAsync(true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ICollection<CategoryForListDto> categoriesForReturn = _mapper.Map<ICollection<CategoryForListDto>>(categories);
      return Ok(categoriesForReturn);
    }

    [HttpGet]
    [Route("all")]
    public async Task<IActionResult> GetAllCategoriesAsync()
    {
      ICollection<Category> categories;

      try  
      {
        categories = await _businessManagment.GetCategoriesAsync(true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ICollection<CategoryForListDto> categoriesForReturn = _mapper.Map<ICollection<CategoryForListDto>>(categories);
      return Ok(categoriesForReturn);
    }

    [HttpGet]
    [Route("detail/ByCId")]
    public async Task<IActionResult> GetCategoryByIdAsync(int id)
    {
      Category category = new Category
      {
        Id = id
      };

      Dictionary<string, string> results = await ErrorHandler.CheckCategoryForByIdAsync(category);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        category = await _businessManagment.GetCategoryByIdAsync(id, true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      CategoryForDetailDto categoriesForReturn = _mapper.Map<CategoryForDetailDto>(category);
      return Ok(categoriesForReturn);
    }

    [HttpGet]
    [Route("detail/ByPId")]
    public async Task<IActionResult> GetCategoriesByParentIdAsync(int id)
    {
      Category category = new Category
      {
        ParentId = id
      };

      Dictionary<string, string> results = await ErrorHandler.CheckCategoryForParentIdAsync(category);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      ICollection<Category> categories;
      try
      {
        categories = await _businessManagment.GetCategoriesByParentIdAsync(id, true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ICollection<CategoryForListDto> categoriesForReturn = _mapper.Map<ICollection<CategoryForListDto>>(categories);
      return Ok(categoriesForReturn);
    }

    [HttpPost]
    [Route("add")]
    public async Task<IActionResult> AddAsync([FromBody] CategoryForAddDto categoryForAddDto)
    {
      Category category = _mapper.Map<Category>(categoryForAddDto);

      Dictionary<string, string> results = await ErrorHandler.CheckCategoryForAddAndUpdateAsync(category);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.AddAsync(categoryForAddDto);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(201);
    }

    [HttpPost]
    [Route("addRange")]
    public async Task<IActionResult> AddRangeAsync([FromBody] ICollection<CategoryForAddDto> categories)
    {

      ICollection<Category> categoryList = new List<Category>();

      foreach (CategoryForAddDto categoryForAddDto in categories)
      {
        Category category = new Category
        {
          Name = categoryForAddDto.Name,
          Description = categoryForAddDto.Description,
          ParentId = categoryForAddDto.ParentId        
        };        
        categoryList.Add(category);
      }

      Dictionary<string, string> results = await ErrorHandler.CheckCategoryForAddAndUpdateAsync(categoryList);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        _businessManagment.AddRange(categories);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(201);
    }

    [HttpPost]
    [Route("update")]
    public async Task<IActionResult> UpdateAsync([FromBody] CategoryForUpdateDto categoryForUpdateDto)
    {
      Category category = _mapper.Map<Category>(categoryForUpdateDto);

      Dictionary<string, string> results = await ErrorHandler.CheckCategoryForAddAndUpdateAsync(category);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.UpdateAsync(categoryForUpdateDto);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(200);
    }

    [HttpPost]
    [Route("updateRange")]
    public async Task<IActionResult> UpdateRangeAsync([FromBody] ICollection<CategoryForUpdateDto> categories)
    {
      ICollection<Category> categoryList = new List<Category>();

      foreach (CategoryForUpdateDto categoryForUpdate in categories)
      {
        Category category = new Category
        {
          Id = categoryForUpdate.Id,
          Name = categoryForUpdate.Name,
          Description = categoryForUpdate.Description,
          ParentId = categoryForUpdate.ParentId
        };
        categoryList.Add(category);
      }     

      Dictionary<string, string> results = await ErrorHandler.CheckCategoryForAddAndUpdateAsync(categoryList);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        _businessManagment.UpdateRange(categories);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(200);
    }

    [HttpDelete]
    [Route("delete/ByCId")]
    public async Task<IActionResult>  DeleteByIdAsync(int id)
    {
      Category category = await _businessManagment.GetCategoryByIdAsync(id);
      if (category == null)
      {
        category = new Category
        {
          Id = id
        };
      }

      Dictionary<string, string> results = await ErrorHandler.CheckCategoryForDeleteAsync(category);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        _businessManagment.DeleteCategoryByIdAsync(id);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return Ok();
    }

    [HttpDelete]
    [Route("deleteRange")]
    public async Task<IActionResult> DeleteRangeAsync([FromBody] ICollection<Category> categories)
    {
      
      Dictionary<string, string> results = await ErrorHandler.CheckCategoryForDeleteAsync(categories);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        _businessManagment.DeleteCategoryRange(categories);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return Ok();
    }
    protected ModelStateDictionary PrepareModelStateForErrors(Dictionary<string, string> errors)
    {
      if (errors.Count != 0)
      {
        foreach (KeyValuePair<string, string> error in errors)
        {
          ModelState.AddModelError(error.Key, error.Value);
        }
        return ModelState;
      }

      return null;
    }

  }
}