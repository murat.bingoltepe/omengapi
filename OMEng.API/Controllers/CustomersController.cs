﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OMEng.API.Dtos.CustomerDtos;
using OMEng.API.Handler.ErrorHandler.Implements;
using OMEng.API.Models;
using OMEng.API.Services.Interfaces;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class CustomersController : ControllerBase
  {
    private readonly IBusinessManagment _businessManagment;
    private readonly IMapper _mapper;

    public CustomersController(IBusinessManagment businessManagment, IMapper mapper)
    {
      _businessManagment = businessManagment;
      _mapper = mapper;
    }

    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetCustomersAsync()
    {
      ICollection<Customer> customers;

      try
      {
        customers = await _businessManagment.GetCustomersAsync(true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ICollection<CustomerForListDto> customersToReturn = _mapper.Map<ICollection<CustomerForListDto>>(customers);
      return Ok(customersToReturn);
    }

    [HttpGet]
    [Route("detail/ByCId")]
    public async Task<IActionResult> GetCustomerByIdAsync(int id)
    {
      Customer customer = new Customer
      {
        Id = id
      };

      Dictionary<string, string> results = await ErrorHandler.CheckCustomerForByIdAsync(customer);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        customer = await _businessManagment.GetCustomerByIdAsync(id, true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      CustomerForDetailDto customerToReturn = _mapper.Map<CustomerForDetailDto>(customer);
      return Ok(customerToReturn);
    }

    [HttpPost]
    [Route("add")]
    public async Task<IActionResult> AddAsync([FromBody] CustomerForAddDto customerForAddDto)
    {
      Customer customer = _mapper.Map<Customer>(customerForAddDto);
      Dictionary<string, string> results = await ErrorHandler.CheckCustomerForAddAsync(customer);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.AddAsync(customerForAddDto);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(201);
    }

    [HttpPost]
    [Route("update")]
    public async Task<IActionResult> UpdateAsync([FromBody] CustomerForUpdateDto customerForUpdateDto)
    {
      Customer customer = _mapper.Map<Customer>(customerForUpdateDto);
      Dictionary<string, string> results = await ErrorHandler.CheckCustomerForUpdateAsync(customer);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.UpdateAsync(customerForUpdateDto);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }
      return StatusCode(200);
    }

    [HttpDelete]
    [Route("delete/ByCId")]
    public async Task<IActionResult> DeleteByIdAsync(int id)
    {
      Customer customer = await _businessManagment.GetCustomerByIdAsync(id);
      if (customer == null)
      {
        customer = new Customer
        {
          Id = id
        };
      }

      Dictionary<string, string> results = await ErrorHandler.CheckCustomerForDeleteAsync(customer);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        _businessManagment.DeleteCustomerByIdAsync(id);        
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return Ok();
    }

    protected ModelStateDictionary PrepareModelStateForErrors(Dictionary<string, string> errors)
    {
      if (errors.Count != 0)
      {
        foreach (KeyValuePair<string, string> error in errors)
        {
          ModelState.AddModelError(error.Key, error.Value);
        }
        return ModelState;
      }

      return null;
    }
  }
}