﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OMEng.API.Dtos.EmployeeDtos;
using OMEng.API.Handler.ErrorHandler.Implements;
using OMEng.API.Models;
using OMEng.API.Services.Interfaces;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class EmployeesController : ControllerBase
  {
    private readonly IBusinessManagment _businessManagment;
    private readonly IMapper _mapper;

    public EmployeesController(IBusinessManagment businessManagment, IMapper mapper)
    {
      _businessManagment = businessManagment;
      _mapper = mapper;
    }

    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetEmployeesAsync()
    {
      ICollection<Employee> employees;

      try
      {
        employees = await _businessManagment.GetEmployeesAsync(true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      ICollection<EmployeeForListDto> employeesForReturn = _mapper.Map<ICollection<EmployeeForListDto>>(employees);
      return Ok(employeesForReturn);
    }

    [HttpGet]
    [Route("detail/ByEId")]
    public async Task<IActionResult> GetEmployeeByIdAsync(int id)
    {
      Employee employee = new Employee
      {
        Id = id
      };

      Dictionary<string, string> results = await ErrorHandler.CheckEmployeeForByIdAsync(employee);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        employee = await _businessManagment.GetEmployeeByIdAsync(id, true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      EmployeeForDetailDto employeeForDetail = _mapper.Map<EmployeeForDetailDto>(employee);
      return Ok(employeeForDetail);
    }

    [HttpGet]
    [Route("detail/ByRId")]
    public async Task<IActionResult> GetEmployeesByRoleIdAsync(int id)
    {
      Employee employee = new Employee
      {
        UserRoleId = id
      };

      Dictionary<string, string> results = await ErrorHandler.CheckEmployeeForListByRoleIdAsync(employee);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      ICollection<Employee> employees;
      try
      {
        employees = await _businessManagment.GetEmployeesByRoleIdAsync(id, true);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);

      }

      ICollection<EmployeeForListDto> employeesToReturn = _mapper.Map<ICollection<EmployeeForListDto>>(employees);
      return Ok(employeesToReturn);
    }

    [HttpPost]
    [Route("add")]
    public async Task<IActionResult> AddAsync([FromBody] EmployeeForAddDto employeeForAddDto)
    {
      Employee employeeToAdd = _mapper.Map<Employee>(employeeForAddDto);
      Dictionary<string, string> results = await ErrorHandler.CheckEmployeeForAddAsync(employeeToAdd);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.AddAsync(employeeForAddDto);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(201);
    }

    [HttpPost]
    [Route("update")]
    public async Task<IActionResult> UpdateAsync([FromBody] EmployeeForUpdateDto employeeForUpdateDto)
    {      
      Employee employee = _mapper.Map<Employee>(employeeForUpdateDto);
      Dictionary<string, string> results = await ErrorHandler.CheckEmployeeForUpdateAsync(employee);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        await _businessManagment.UpdateAsync(employeeForUpdateDto);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return StatusCode(200);
    }

    [HttpDelete]
    [Route("delete/ByEId")]
    public async Task<IActionResult> DeleteByIdAsync(int id)
    {
      Employee employee = await _businessManagment.GetEmployeeByIdAsync(id);
      if (employee == null)
      {
        employee = new Employee
        {
          Id = id
        };
      }

      Dictionary<string, string> results = await ErrorHandler.CheckEmployeeForDeleteAsync(employee);

      ModelStateDictionary errors = PrepareModelStateForErrors(results);
      if (errors != null)
      {
        return BadRequest(errors);
      }

      try
      {
        _businessManagment.DeleteEmployeeByIdAsync(id);
      }      
      catch (Exception e)
      {
        ModelState.AddModelError("ErrorMessage", e.Message);
        return BadRequest(ModelState);
      }

      return Ok();
    }

    protected ModelStateDictionary PrepareModelStateForErrors(Dictionary<string, string> errors)
    {
      if (errors.Count != 0)
      {
        foreach (KeyValuePair<string, string> error in errors)
        {
          ModelState.AddModelError(error.Key, error.Value);
        }
        return ModelState;
      }

      return null;
    }
  }
}