﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos;
using OMEng.API.Models;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class UsersController : ControllerBase
  {
    private IAppRepository _appRepository;
    private IUserRepository _userRepository;
    private IMapper _mapper;

    public UsersController(IAppRepository appRepository, IUserRepository userRepository, IMapper mapper)
    {
      _appRepository = appRepository;
      _userRepository = userRepository;
      _mapper = mapper;
    }

    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetUsersAsync()
    {
      ICollection<User> users;

      try
      {
        users = await _userRepository.GetUsersAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      ICollection<UserDto> usersToReturn = _mapper.Map<ICollection<UserDto>>(users);
      return Ok(usersToReturn);
    }

    [HttpGet]
    [Route("detail/ByUId")]
    public async Task<IActionResult> GetUserByIdAsync(int id)
    {
      User user;

      try
      {
        user = await _userRepository.GetUserByIdAsync(id);
      }
      catch (Exception e)
      {
        throw e;
      }

      UserDto userToReturn = _mapper.Map<UserDto>(user);
      return Ok(userToReturn);
    }

    [HttpGet]
    [Route("detail/ByRId")]
    public async Task<IActionResult> GetUsersByRoleIdAsync(int id)
    {
      ICollection<User> users;

      try
      {
        users = await _userRepository.GetUsersByRoleIdAsync(id);
      }
      catch (Exception e)
      {
        throw e;
      }

      ICollection<UserDto> usersToReturn = _mapper.Map<ICollection<UserDto>>(users);
      return Ok(usersToReturn);
    }

    [HttpPost]
    [Route("add")]
    public async Task<IActionResult> AddAsync([FromBody] User user)
    {
      try
      {
        await _appRepository.AddAsync(user);
        await _appRepository.SaveAllAsync();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok(user);
    }

    [HttpPost]
    [Route("update")]
    public ActionResult Update([FromBody] User user)
    {
      try
      {
        _appRepository.Update(user);
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok(user);
    }

    [HttpDelete]
    [Route("delete")]
    public ActionResult Delete([FromBody] User user)
    {
      try
      {
        _appRepository.Delete(user);
        _appRepository.SaveAll();
      }
      catch (Exception e)
      {
        throw e;
      }

      return Ok();
    }
  }
}