﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Dtos.CustomerDtos;
using OMEng.API.Models;

namespace OMEng.API.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  [ApiController]
  public class CustomerAuthController : ControllerBase
  {
    private readonly ICustomerAuthRepository _customerAuthRepository;
    private readonly IConfiguration _configuration;
    public CustomerAuthController(ICustomerAuthRepository customerAuthRepository, IConfiguration configuration)
    {
      _customerAuthRepository = customerAuthRepository;
      _configuration = configuration;
    }

    [HttpPost]
    [Route("register")]
    public async Task<IActionResult> Register([FromBody] CustomerForRegisterDto customerForRegisterDto)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (await _customerAuthRepository.UserExist(customerForRegisterDto.UserName))
      {
        ModelState.AddModelError("UserName", "Username already exist");
      }

      try
      {
        Customer customerToCreate = new Customer
        {
          UserName = customerForRegisterDto.UserName
        };

        await _customerAuthRepository.Register(customerToCreate, customerForRegisterDto.Password);
      }
      catch (Exception e)
      {
        throw e;
      }

      //Create aksiyonu oldugu icin, create denke gelen 201 kodu 
      //geri gönderilmektedir.
      return StatusCode(201);
    }

    [HttpPost]
    [Route("login")]
    public async Task<ActionResult> Login([FromBody] CustomerForLoginDto customerForLoginDto)
    {
      Customer customer = await _customerAuthRepository.Login(customerForLoginDto.UserName, customerForLoginDto.Password);

      if (customer == null)
      {
        return Unauthorized();
      }

      return Ok(GetSecurityToken(customer));
    }

    [HttpPost]
    [Route("changePswd")]
    public async Task<ActionResult> ChangePassword([FromBody] CustomerForLoginDto customer)
    {
      Customer customerToUpdate = new Customer
      {
        UserName = customer.UserName
      };

      Customer updatedCustomer = await _customerAuthRepository.ChangePassword(customerToUpdate);

      return Ok(updatedCustomer);
    }

    private string GetSecurityToken(Customer customer)
    {
      byte[] key = Encoding.ASCII.GetBytes(_configuration.GetSection("AppSettings:Token").Value);
      SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(new Claim[]
        {
          new Claim(ClaimTypes.NameIdentifier, customer.Id.ToString()),
          new Claim(ClaimTypes.Name, customer.UserName)
        }),
        //Kullanici ne kadar sistemde kalabilir
        Expires = DateTime.Now.AddDays(1),
        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
      };

      // Tokenler login olan kullanicinin haklari ile alakali bilgileri tutar
      JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

      SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
      string tokenString = tokenHandler.WriteToken(token);

      return tokenString;
    }
  }
}