﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class Address
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("Id")]
    public int Id { get; set; }

    [Column("Street")]
    [StringLength(100, ErrorMessage = "Street cannot be longer than 100 characters.")]
    public string Street { get; set; }

    [Column("Number")]
    [StringLength(10, ErrorMessage = "Number cannot be longer than 10 characters.")]
    public string Number { get; set; }

    [Column("PostalCode")]
    [StringLength(10, ErrorMessage = "PostalCode cannot be longer than 10 characters.")]
    public string PostalCode { get; set; }

    [Column("City")]
    [StringLength(50, ErrorMessage = "City cannot be longer than 50 characters.")]
    public string City { get; set; }

    [Column("Country")]
    [StringLength(100, ErrorMessage = "Contry cannot be longer than 100 characters.")]
    public string Country { get; set; }
  }
}
