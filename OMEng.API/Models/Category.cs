﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class Category
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("Id")]
    public int Id { get; set; }

    [Required(ErrorMessage = "Name cannot be empty")]
    [Column("Name")]
    [StringLength(150, ErrorMessage = "Name cannot be longer than 150 and shorter than 3 characters.", MinimumLength = 3)]
    public string Name { get; set; }

    [Column("Description")]
    [StringLength(300, ErrorMessage = "Description cannot be longer than 300 and shorter than 3 characters.")]
    public string Description { get; set; }

    [Column("ParentId")]    
    public int? ParentId { get; set; }

    [Column("IsParent", TypeName = "TINYINT(1)")]
    public bool IsParent { get; set; }
  }
}
