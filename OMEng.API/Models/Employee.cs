﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class Employee : User
  {
    public Employee()
    {
      Products = new List<Product>();
    }

    [Column("SocialSecurityNumber")]
    [StringLength(50, ErrorMessage = "Social security number cannot be longer than 50 and shorter than 3 characters.", MinimumLength = 3)]
    public string SocialSecurityNumber { get; set; }

    public virtual ICollection<Product> Products { get; set; }
  }
}
