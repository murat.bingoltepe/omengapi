﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
	public class OrderDetail
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("Id")]
		public int Id { get; set; }


		[Column("Quantity")]
		public int Quantity { get; set; }


		[ForeignKey("fk_order_details_order")]
		[Column("OrderId")]
		public int OrderId { get; set; }
		public virtual Order Order { get; set; }


		[ForeignKey("fk_order_details_product")]
		[Column("ProductId")]
		public int ProductId { get; set; }
		public virtual Product Product { get; set; }
	}
}
