﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class Customer : User
  {
    [Column("TaxId")]
    [StringLength(50, ErrorMessage = "TaxId cannot be longer than 50 and shorter than 3 characters.", MinimumLength = 3)]
    public string TaxId { get; set; }

    [Column("CompanyName")]
    [StringLength(150, ErrorMessage = "Company name cannot be longer than 150 and shorter than 3 characters.", MinimumLength = 3)]
    public string CompanyName { get; set; }

    [Column("PositionByCompany")]
    [StringLength(50, ErrorMessage = "Position by Company cannot be longer than 50 and shorter than 3 characters.", MinimumLength = 3)]
    public string PositionByCompany { get; set; }
    
  }
}
