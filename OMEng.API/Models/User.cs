﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public abstract class User: Person
  {
    [ForeignKey("fk_users_address")]
    [Column("AddressId")]
    public int AddressId { get; set; }
    public virtual Address Address { get; set; }


    [ForeignKey("fk_users_userrole")]
    [Column("UserRoleId")]
    public int UserRoleId { get; set; }
    public virtual UserRole UserRole { get; set; }


    [ForeignKey("fk_users_account")]
    [Column("AccountId")]
    public int AccountId { get; set; }
    public virtual Account Account { get; set; }
  }
}
