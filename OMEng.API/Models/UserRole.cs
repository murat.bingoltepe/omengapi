﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class UserRole
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("Id")]
    public int Id { get; set; }

    [Required(ErrorMessage = "Name cannot be empty")]
    [Column("Name")]
    [StringLength(50, ErrorMessage = "Name cannot be longer than 50 and shorter than 5 characters.", MinimumLength = 5)]
    public string Name { get; set; }

    [Column("Description")]
    [StringLength(200, ErrorMessage = "Name cannot be longer than 200 characters.")]
    public string Description { get; set; }
  }
}
