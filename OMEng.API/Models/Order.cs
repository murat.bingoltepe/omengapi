﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class Order
  {
		public Order()
		{
      OrderDetails = new List<OrderDetail>();
		}

		[Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("Id")]
    public int Id { get; set; }


    [Required(ErrorMessage = "OrderNumber cannot be empty")]
    [Column("OrderNumber")]
    [StringLength(45, ErrorMessage = "OrderNumber cannot be longer than 45 and shorter than 10 characters.", MinimumLength = 10)]
    public string OrderNumber { get; set; }


    [Column("Description")]
    [StringLength(2000, ErrorMessage = "Description cannot be longer than 2000 characters.")]
    public string Description { get; set; }


    [Timestamp]
    [Column("AddedTimeStamp")]
    public DateTime AddedTimeStamp { get; set; }


    [Timestamp]
    [Column("UpdatedTimeStamp")]
    public DateTime UpdatedTimeStamp { get; set; }


    [Required(ErrorMessage = "State cannot be empty")]
    [Column("State", TypeName = "TINYINT(3)")]
    [Range(0, 999, ErrorMessage = "ID number cannot be longer than 3 digits")]
    [StringLength(3, MinimumLength = 1, ErrorMessage = "State cannot be longer than 3 and shorter than 1 char.")]
    public int State { get; set; }

    [Required(ErrorMessage = "Customer cannot be empty")]
    [ForeignKey("fk_orders_customer")]
    [Column("CustomerId")]
    public int CustomerId { get; set; }
    public virtual Customer Customer { get; set; }   
    

    public virtual ICollection<OrderDetail> OrderDetails { get; set; }
  }
}
