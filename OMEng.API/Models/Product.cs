﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class Product
  {
    public Product()
    {
      Photos = new List<Photo>();
    }

    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("Id")]
    public int Id { get; set; }


    [Required(ErrorMessage = "Name cannot be empty")]
    [Column("Name")]
    [StringLength(300, ErrorMessage = "Name cannot be longer than 300 and shorter than 5 characters.", MinimumLength = 5)]
    public string Name { get; set; }


    [Column("Description")]
    [StringLength(2000, ErrorMessage = "Description cannot be longer than 2000 characters.")]
    public string Description { get; set; }


    [Column("QuantityPerUnit")]
    public int QuantityPerUnit { get; set; }


    [Column("UnitPrice", TypeName = "DECIMAL(15,2)")]
    [DataType(DataType.Currency)]    
    public decimal UnitPrice { get; set; }


    [Timestamp]
    [Column("AddedTimeStamp")]
    public DateTime AddedTimeStamp { get; set; }


    [Timestamp]
    [Column("UpdatedTimeStamp")]
    public DateTime UpdatedTimeStamp { get; set; }


    [ForeignKey("fk_products_employee")]
    [Column("EmployeeId")]
    public int EmployeeId { get; set; }
    public virtual Employee Employee { get; set; }


    [ForeignKey("fk_products_category")]
    [Column("CategoryId")]
    public int CategoryId { get; set; }
    public virtual Category Category { get; set; }


    public virtual ICollection<Photo> Photos { get; set; }
    
  }
}
