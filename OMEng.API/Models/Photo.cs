﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class Photo
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("Id")]
    public int Id { get; set; }


    [Column("Description")]
    [StringLength(2000, ErrorMessage = "Description cannot be longer than 2000 characters.")]
    public string Description { get; set; }


    [Timestamp]
    [Column("AddedTimeStamp")]
    public DateTime AddedTimeStamp { get; set; }


    [Timestamp]
    [Column("UpdatedTimeStamp")]
    public DateTime UpdatedTimeStamp { get; set; }


    [Column("IsMain", TypeName = "TINYINT(1)")]
    public bool IsMain { get; set; }


    [Column("Path")]
    [StringLength(1000, ErrorMessage = "Path cannot be longer than 1000 characters.")]
    public string Path { get; set; }
    

    [ForeignKey("fk_photos_product")]
    [Column("ProductId")]
    public int ProductId { get; set; }
    public virtual Product Product { get; set; }


    [ForeignKey("fk_photos_employee")]
    [Column("EmployeeId")]
    public int EmployeeId { get; set; }
    public virtual Employee Employee { get; set; }
  }
}
