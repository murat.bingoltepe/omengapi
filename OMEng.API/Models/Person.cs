﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public abstract class Person
  {
    [Key]   
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]    
    [Column("Id")]    
    public int Id { get; set; }

    
    [Required(ErrorMessage = "User name cannot be empty")]
    [Column("UserName")]
    [StringLength(30, ErrorMessage = "User name cannot be longer than 30 and shorter than 3 characters.", MinimumLength = 3)]
    public string UserName { get; set; }


    [Required]
    [Column("PasswordHash")]
    public byte[] PasswordHash { get; set; }


    [Required]
    [Column("PasswordSalt")]
    public byte[] PasswordSalt { get; set; }

  
    [Required(ErrorMessage = "First name cannot be empty")]
    [Column("FirstName")]
    [StringLength(50, ErrorMessage = "First name cannot be longer than 50 and shorter than 3 characters.", MinimumLength = 3)]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "Last name cannot be empty")]
    [Column("LastName")]
    [StringLength(50, ErrorMessage = "Last name cannot be longer than 50 and shorter than 3 characters.", MinimumLength = 3)]
    public string LastName { get; set; }


    [Column("Title")]
    [StringLength(30, ErrorMessage = "Title cannot be longer than 30 characters.")]
    public string Title { get; set; }


    [Required(ErrorMessage = "Gender cannot be empty")]
    [Column("Gender")]
    [StringLength(20, ErrorMessage = "Gender cannot be longer than 20 and shorter than 1 characters.", MinimumLength = 1)]    
    public string Gender { get; set; }


    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
    [Column("BirthDate")]
    public DateTime BirthDate { get; set; }


    [Column("Tel")]
    [StringLength(40, ErrorMessage = "Tel cannot be longer than 40 and shorter than 10 characters.", MinimumLength = 10)]
    public string Tel { get; set; }


    [Column("Mobile")]
    [StringLength(40, ErrorMessage = "Mobile cannot be longer than 40 and shorter than 10 characters.", MinimumLength = 10)]
    public string Mobile { get; set; }


    [Column("Email")]
    [StringLength(60, ErrorMessage = "Email cannot be longer than 60 and shorter than 6 characters.", MinimumLength = 6)]
    [DataType(DataType.EmailAddress, ErrorMessage = "Invalid email")]
    public string Email { get; set; }


    [Column("TempPassword", TypeName = "TINYINT(1)")]
    public bool TempPassword { get; set; }


    [Column("EmailConfirmed", TypeName = "TINYINT(1)")]
    public bool EmailConfirmed { get; set; }


    [Timestamp]
    [Column("AddedTimeStamp")]
    public DateTime AddedTimeStamp { get; set; }

    [Timestamp]
    [Column("UpdatedTimeStamp")]
    public DateTime UpdatedTimeStamp { get; set; }

    [Column("Discriminator")]
    [StringLength(50, ErrorMessage = "Discriminator cannot be longer than 50 characters.")]
    public string Discriminator { get; set; }
  }
}
