﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Models
{
  public class Account
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("Id")]
    public int Id { get; set; }


    [Column("Iban")]
    [StringLength(30, ErrorMessage = "Iban cannot be longer than 30 characters.")]
    public string Iban { get; set; }


    [Column("Swift")]
    [StringLength(20, ErrorMessage = "Swift cannot be longer than 20 characters.")]
    public string Swift { get; set; }


    [Column("BankName")]
    [StringLength(50, ErrorMessage = "Bank name cannot be longer than 50 characters.")]
    public string BankName { get; set; }


    [Column("BankCode")]
    [StringLength(20, ErrorMessage = "Bank code cannot be longer than 20 characters.")]
    public string BankCode { get; set; }


    [Column("AccountNumber")]
    [StringLength(20, ErrorMessage = "Account number cannot be longer than 20 characters.")]
    public string AccountNumber { get; set; }    
  }
}
