﻿using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using OMEng.API.Data;
using OMEng.API.Data.InterfaceRepository;
using OMEng.API.Data.Repository;
using OMEng.API.Services.Implements;
using OMEng.API.Services.Interfaces;

namespace OMEng.API
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.Configure<IISOptions>(options =>
      {

      });

      var key = Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value);

      services.AddDbContext<DataContext>(options => options.UseMySQL(Configuration.GetConnectionString("DefaultConnection"))
      .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));

      services.AddAutoMapper();
      services.AddMvc(options=>options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Latest);      
      services.AddControllersWithViews().AddNewtonsoftJson(options =>
        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

      services.AddCors();
      services.AddScoped<IAddressRepository, AddressRepository>();
      services.AddScoped<IAccountRepository, AccountRepository>();
      services.AddScoped<IAppRepository, AppRepository>();
      services.AddScoped<ICustomerAuthRepository, CustomerAuthRepository>();
      services.AddScoped<ICustomerRepository, CustomerRepository>();
      services.AddScoped<IEmployeeAuthRepository, EmployeeAuthRepository>();
      services.AddScoped<IEmployeeRepository, EmployeeRepository>();
      services.AddScoped<IPhotoRepository, PhotoRepository>();
      services.AddScoped<IProductRepository, ProductRepository>();
      services.AddScoped<ICategoryRepository, CategoryRepository>();
      services.AddScoped<IUserRepository, UserRepository>();
      services.AddScoped<IUserRoleRepository, UserRoleRepository>();
      services.AddScoped<IBusinessManagment, BusinessManagment>();

      services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt =>
      {
        opt.TokenValidationParameters = new TokenValidationParameters
        {
          ValidateIssuerSigningKey = true,
          IssuerSigningKey = new SymmetricSecurityKey(key),

          // Public bir proje oldugu icin asagidaki degiskenler false degerini aliyor
          ValidateIssuer = false,
          ValidateAudience = false
        };
      });
    }


    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      app.UseStaticFiles();

      if (Environments.Development.Equals(env.EnvironmentName))
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseHsts();
      }

      //app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
      app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
      app.UseHttpsRedirection();
      app.UseAuthentication();
      app.UseMvc();
    }
  }
}
