﻿using OMEng.API.Models;
using System;

namespace OMEng.API.Dtos
{
  public class UserDto
  {
    public int Id { get; set; }
    public string UserName { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public bool TempPassword { get; set; }
    public DateTime AddedTimeStamp { get; set; }
    public string TaxId { get; set; }
    public string CompanyName { get; set; }
    public string PositionByCompany { get; set; }
    public string SocialSecurityNumber { get; set; }

    public Address Address { get; set; }
    public UserRole UserRole { get; set; }
  }
}
