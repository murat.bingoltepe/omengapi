﻿using OMEng.API.Models;
using System;
using System.Collections.Generic;

namespace OMEng.API.Dtos.OrderDtos
{
	public class OrderForDetailDto
	{
		public int Id { get; set; }
		public string OrderNumber { get; set; }
		public string Description { get; set; }
		public DateTime AddedTimeStamp { get; set; }
		public DateTime UpdatedTimeStamp { get; set; }
		public int State { get; set; }

		public Customer Customer { get; set; }

		public ICollection<OrderDetail> OrderDetails { get; set; }
	}
}
