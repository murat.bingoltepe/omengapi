﻿namespace OMEng.API.Dtos.OrderDtos
{
	public class OrderForUpdateDto
	{
		public int Id { get; set; }
		public string OrderNumber { get; set; }
		public string Description { get; set; }
		public int State { get; set; }

		public int CustomerId { get; set; }
	}
}
