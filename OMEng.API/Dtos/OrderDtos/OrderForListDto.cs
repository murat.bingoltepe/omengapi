﻿using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Dtos.OrderDtos
{
	public class OrderForListDto
	{
		public int Id { get; set; }
		public string OrderNumber { get; set; }
		public string Description { get; set; }
		public int State { get; set; }

		public Customer Customer { get; set; }
	}
}
