﻿
namespace OMEng.API.Dtos.OrderDtos
{
	public class OrderForAddDto
	{
		public string OrderNumber { get; set; }
		public string Description { get; set; }
		public int State { get; set; }

		public int CustomerId { get; set; }
	}
}
