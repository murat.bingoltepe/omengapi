﻿namespace OMEng.API.Dtos
{
  public class UserRoleDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
  }
}
