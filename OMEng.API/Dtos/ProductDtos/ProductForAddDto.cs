﻿using OMEng.API.Models;
using System;
using System.Collections.Generic;

namespace OMEng.API.Dtos.ProductDtos
{
  public class ProductForAddDto
  {
    public string Name { get; set; }
    public string Description { get; set; }
    public int QuantityPerUnit { get; set; }
    public decimal UnitPrice { get; set; }

    public int CategoryId { get; set; }
    public int EmployeeId { get; set; }
      
  }
}
