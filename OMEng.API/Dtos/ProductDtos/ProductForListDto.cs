﻿using OMEng.API.Models;
using System;
using System.Collections.Generic;

namespace OMEng.API.Dtos.ProductDtos
{
  public class ProductForListDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string PhotoUrl { get; set; }

    public Category Category { get; set; }
    public Employee Employee { get; set; }        
  }
}
