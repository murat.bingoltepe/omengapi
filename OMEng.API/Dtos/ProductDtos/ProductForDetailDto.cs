﻿using OMEng.API.Models;
using System;
using System.Collections.Generic;

namespace OMEng.API.Dtos.ProductDtos
{
  public class ProductForDetailDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int QuantityPerUnit { get; set; }
    public decimal UnitPrice { get; set; }
    public DateTime AddedTimeStamp { get; set; }
    public DateTime UpdatedTimeStamp { get; set; }

    public Category Category { get; set; }
    public Employee Employee { get; set; }

    public ICollection<Photo> Photos { get; set; }
  }
}
