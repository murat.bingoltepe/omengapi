﻿namespace OMEng.API.Dtos.CustomerDtos
{
  public class CustomerForLoginDto
  {
    public string UserName { get; set; }
    public string Password { get; set; }
  }
}
