﻿using OMEng.API.Models;
using System;

namespace OMEng.API.Dtos.CustomerDtos
{
  public class CustomerForDetailDto
  {
    public int Id { get; set; }
    public string UserName { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Title { get; set; }
    public string Gender { get; set; }
    public DateTime BirthDate { get; set; }
    public string Tel { get; set; }
    public string Mobile { get; set; }
    public string Email { get; set; }
    public DateTime AddedTimeStamp { get; set; }
    public DateTime UpdatedTimeStamp { get; set; }
    public string TaxId { get; set; }
    public string CompanyName { get; set; }
    public string PositionByCompany { get; set; }
    public string Discriminator { get; set; }

    public Address Address { get; set; }    
    public Account Account { get; set; }    
  }
}
