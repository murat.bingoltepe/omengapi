﻿using OMEng.API.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Dtos
{
  public class PhotoDto
  {
    public int Id { get; set; }
    public string Description { get; set; }
    public DateTime AddedTimeStamp { get; set; }
    [Column(TypeName = "TINYINT(1)")]
    public bool IsMain { get; set; }
    public string Path { get; set; }

    public Product Product { get; set; }
    public Employee Employee { get; set; }
  }
}

