﻿using OMEng.API.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMEng.API.Dtos.CategoryDtos
{
  public class CategoryForDetailDto
  {
    public int Id { get; set; }    
    public string Name { get; set; }
    public string Description { get; set; }
    public int ParentId { get; set; }
    [Column("IsParent", TypeName = "TINYINT(1)")]
    public bool IsParent { get; set; }
  }
}
