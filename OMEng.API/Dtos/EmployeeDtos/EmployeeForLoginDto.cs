﻿namespace OMEng.API.Dtos.EmployeeDtos
{
  public class EmployeeForLoginDto
  {
    public string UserName { get; set; }
    public string Password { get; set; }
  }
}
