﻿using OMEng.API.Models;
using System;

namespace OMEng.API.Dtos.EmployeeDtos
{
  public class EmployeeForListDto
  {
    public int Id { get; set; }
    public string UserName { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Title { get; set; }    
    public string Gender { get; set; }
    public string Discriminator { get; set; }

    public UserRole UserRole { get; set; }  
    public Address Address { get; set; }
  }
}
