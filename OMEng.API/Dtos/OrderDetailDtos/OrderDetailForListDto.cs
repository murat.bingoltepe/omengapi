﻿using OMEng.API.Models;

namespace OMEng.API.Dtos.OrderDetailDtos
{
	public class OrderDetailForListDto
	{
		public int Id { get; set; }
		public int Quantity { get; set; }
		public Order Order { get; set; }
		public Product Product { get; set; }
	}
}
