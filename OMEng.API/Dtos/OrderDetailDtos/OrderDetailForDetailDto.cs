﻿using OMEng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Dtos.OrderDetailDtos
{
	public class OrderDetailForDetailDto
	{
		public int Id { get; set; }
		public int Quantity { get; set; }
		public Order Order { get; set; }
		public Product Product { get; set; }
	}
}
