﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OMEng.API.Dtos.OrderDetailDtos
{
	public class OrderDetailForUpdate
	{
		public int Id { get; set; }
		public int Quantity { get; set; }
		public int OrderId { get; set; }
		public int ProductId { get; set; }
	}
}
