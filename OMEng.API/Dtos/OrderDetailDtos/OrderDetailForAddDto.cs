﻿
namespace OMEng.API.Dtos.OrderDetailDtos
{
	public class OrderDetailForAddDto
	{		
		public int Quantity { get; set; }
		public int OrderId { get; set; }
		public int ProducktId { get; set; }
	}
}
